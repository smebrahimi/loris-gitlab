import Vue from 'vue'
import lodash from 'lodash'
import VueLodash from 'vue-lodash'
import VueResource from 'vue-resource'
import Notifications from 'vue-notification'
import 'vue-awesome/icons/flag'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
import BootstrapVue from 'bootstrap-vue'
import VeeValidate, { Validator } from 'vee-validate'
import validationMessagesFA from 'vee-validate/dist/locale/fa'

import store from '@/store'
import i18n from '@/i18n'
import Config from '@/config/config'
// eslint-disable-next-line
import mixins from '@/mixins/index'

import App from '@main/App.vue'
import router from '@main/router'
import AppLayout from '@main/layout/app.vue'
// eslint-disable-next-line
import localStore from '@main/store'
// eslint-disable-next-line
import filters from '@/filters'
import EntitiesMixin from '@/mixins/entities'

import VueAnalytics from 'vue-analytics'
import 'owl.carousel/dist/assets/owl.carousel.css'
import 'owl.carousel'

Validator.localize('fa', validationMessagesFA)
Vue.use(VeeValidate)
Vue.use(BootstrapVue)
Vue.use(require('vue-moment'))
Vue.component('appLayout', AppLayout)
Vue.component('icon', Icon)
Vue.use(VueLodash, lodash)
Vue.use(Notifications)
Vue.use(VueResource)
Vue.use(VueAnalytics, {
  id: Config.thirdPartyCredentials.googleAnalytics,
  router
})
Vue.mixin(EntitiesMixin)
Vue.config.productionTip = Config.productionTip

Vue.http.options.root = Config.apiUrl
Vue.http.interceptors.push((request, next) => {
  const token = store.state.auth.token
  if (store.state.auth.isLogin) {
    request.headers.set('X-CSRF-TOKEN', 'TOKEN')
    request.headers.set('Authorization', `${token}`)
  }
  next((response) => {
    if (response.status === 401) {
      const requestUrl = request.url

      /*
        following URLs are OK to respond 401 (user should not be logged out)

        'users/{id}/sessions'
        'programs/{id}/urlAccess'
        'programs/{id}/reportPlayback'
      */

      // todo: do this in correct way
      if (requestUrl.indexOf('/sessions') === -1 && requestUrl.indexOf('/urlAccess') === -1 && requestUrl.indexOf('/reportPlayback') === -1) {
        store.commit('auth/logout')

        router.push({ name: 'Login' })
      }
    }
  })
})

/* eslint-disable no-new */
export const app = new Vue({
  el: '#app',
  components: {
    App,
    Icon
  },
  render: h => h(App),
  store: store,
  router,
  i18n,
  template: '<App/>'
})
