export default {
  mounted: function () {
    // reset scroll after page change
    window.scrollTo(0, 0)
    this.setDefaultMetaKeys()
    this.updateSeoInfo()
  }
}
