import Vue from 'vue'
Vue.mixin({
  computed: {
    showMainLayoutSections () {
      return this.$route.path !== '/login' && !this.isEmbedded
    },
    isEmbedded () {
      return this.$route.query.embeded
    },
    isLogin () {
      return this.authIsLogin
    },
    currentUser () {
      return this.authMemberInfo
    },
    isSM () {
      return this.layoutResponsiveSize >= 576
    },
    isMD () {
      return this.layoutResponsiveSize >= 768
    },
    isLG () {
      return this.layoutResponsiveSize >= 992
    },
    isXL () {
      return this.layoutResponsiveSize >= 1200
    }
  },
  methods: {
    updateSeoInfo () {
      this.setPageTitle()
      this.setHtmlMeta({
        key: 'og:url',
        value: window.location.href,
        propertyKey: 'property'
      })
      this.setHtmlMeta({
        key: 'twitter:url',
        value: window.location.href
      })
    },
    setDefaultMetaKeys () {
      this.setHtmlMeta({
        key: 'keywords'
      })

      this.setHtmlMeta({
        key: 'description'
      })

      this.setDescriptionMetaTag()
    },
    setDescriptionMetaTag (description) {
      const pageDescription = window.document.querySelector('[name="description"]')
      const value = description || pageDescription.content

      this.setHtmlMeta({
        key: 'description',
        value
      })

      this.setHtmlMeta({
        key: 'og:description',
        value,
        propertyKey: 'property'
      })

      this.setHtmlMeta({
        key: 'twitter:description',
        value
      })
    },
    setHtmlMeta ({ key, value, propertyKey = 'name' }) {
      const meta = document.querySelector(`[${propertyKey}="${key}"]`)
      // set default keywords
      if (key === 'keywords' && !value) {
        value = this.$t('head.defaultKeyword')
      } else if (key === 'description' && !value) {
        value = this.$t('head.defaultDescription')
      }
      meta && (meta.content = value || '')
    },
    setPageTitle (title) {
      this.setHtmlMeta({
        key: 'og:title',
        value: document.title,
        propertyKey: 'property'
      })
      this.setHtmlMeta({
        key: 'twitter:title',
        value: document.title
      })
    },
    setLink ({rel, href}) {
      const item = document.querySelector(`[rel="${rel}"]`)
      if (item) {
        if (href) {
          item.href = href
        } else {
          document.head.removeChild(item)
        }
      } else {
        if (!href) return false
        let link = document.createElement('link')
        link.rel = rel
        link.href = href
        document.head.appendChild(link)
      }
    }
  }
})
