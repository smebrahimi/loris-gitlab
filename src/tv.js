// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import store from '@/store'
import App from '@/ui/tv/App.vue'
import router from '@/ui/tv/router'
import VueResource from 'vue-resource'
import NProgress from 'vue-nprogress'
import Notifications from 'vue-notification'
import Config from '@/config/config'
import lodash from 'lodash'
import VueLodash from 'vue-lodash'
import i18n from '@/i18n'
import LorisNavigate from './helpers/loris-navigate'

LorisNavigate()
Vue.use(VueLodash, lodash)
Vue.use(NProgress, Config.NProgress)
Vue.use(Notifications)
Vue.use(VueResource)
Vue.config.productionTip = Config.productionTip
Vue.http.options.root = Config.apiUrl
Vue.http.interceptors.push((request, next) => {
  const token = store.state.auth.token
  request.headers.set('X-CSRF-TOKEN', 'TOKEN')
  request.headers.set('Authorization', `${token}`)
  request.headers.set('Accept', 'application/json')
  next()
})
Vue.http.interceptors.push(function (request, next) {
  next(function (response) {
    if (response.ok === false) {
      // console.log('show notification')
      store.commit('notify/error', {
        message: response.statusText,
        action: response.body.message
      })
    }
  })
})
// console.log(process.env)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: {App},
  render: h => h(App),
  store: store,
  router,
  i18n,
  template: '<App/>'
})
