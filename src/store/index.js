import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import notify from './modules/notify'
import notification from './modules/notification'
import profile from './modules/profile'
import program from './modules/program'
import serverConfig from './modules/server-config'
import programCountdown from './modules/program-countdown'
import mediaPlayer from './modules/mediaplayer'
import userPackage from './modules/user-package'
import aboutUs from './modules/about-us'
import contactUs from './modules/contact-us'
import locale from './modules/locale'
import agent from './modules/agent'
import conductor from './modules/conductor'
import tv from './modules/tv'
import archive from './modules/archive'
import archiveConductor from './modules/archiveConductor'
import tvRoute from './modules/tvRoute'

/* eslint-disable */
let Promise = require('es6-promise').Promise
require('es6-promise').polyfill()
/* eslint-enable */
Vue.use(Vuex)
const store = new Vuex.Store({
  strict: false,
  state: {
    storeVersion: '0.0.0'
  },
  modules: {
    auth: auth,
    notification: notification,
    profile: profile,
    program: program,
    notify: notify,
    serverConfig: serverConfig,
    programCountdown: programCountdown,
    mediaPlayer: mediaPlayer,
    userPackage: userPackage,
    aboutUs: aboutUs,
    contactUs: contactUs,
    locale: locale,
    agent: agent,
    conductor: conductor,
    tv: tv,
    archive: archive,
    archiveConductor: archiveConductor,
    tvRoute: tvRoute
  }
})
export default store
