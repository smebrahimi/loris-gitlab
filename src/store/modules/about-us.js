import Server from '@/services/server'

export default {
  namespaced: true,
  state: {
    contentLoading: false,
    content: null
  },
  getters: {
    contentLoading: (state) => {
      return state.contentLoading
    },
    content: (state) => {
      return state.content
    }
  },
  mutations: {
    updateContent (state, payload) {
      state.content = payload
    },
    updateContentLoading (state, payload) {
      state.contentLoading = payload
    }
  },
  actions: {
    getContent (context) {
      context.commit('updateContentLoading', true)
      const promise = Server.getAboutUs()
      promise.then((response) => {
        context.commit('updateContent', response.bodyText)
        context.commit('updateContentLoading', false)
      }).catch(() => {
        context.commit('updateContentLoading', false)
      })
      return promise
    }
  }
}
