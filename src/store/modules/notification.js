// TODO: complete get notifications
import i18n from '@/i18n'
import Server from '@/services/server'

export default {
  namespaced: true,
  state: {
    count: null,
    list: [],
    listLoading: false
  },
  getters: {
    count: (state) => {
      return state.count
    },
    list: (state) => {
      return state.list
    },
    listLoading: (state) => {
      return state.listLoading
    }
  },
  mutations: {
    update (state, payload) {
      state.count = payload.count
      state.list = payload
    },
    setListLoading (state, payload) {
      state.listLoading = payload
    }
  },
  actions: {
    getAll (context) {
      context.commit('setListLoading', true)
      // TODO complete pagination notification
      const promise = Server.getNotifications({id: context.rootState.auth.memberInfo.id})
      promise.then((response) => {
        context.commit('update', response.body)
        context.commit('setListLoading', false)
      }).catch(() => {
        context.commit('setListLoading', false)
      })
      return promise
    },
    updateLastMessageWhichReadByUser (context, payload) {
      const promise = Server.postLastNotification(
        {id: context.rootState.auth.memberInfo.id},
        {'notificationId': payload})
      promise.then((response) => {
        // console.log(response.state)
      }).catch(() => {
        context.commit('notify/error', {
          message: i18n.t('notifications.someProblem')
        }, {root: true})
      })
      return promise
    }
  }
}
