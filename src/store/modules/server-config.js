import Server from '@/services/server'

export default {
  namespaced: true,
  state: {
    config: null,
    configLoading: false
  },
  getters: {
    config: (state) => {
      return state.config
    },
    configLoading: (state) => {
      return state.configLoading
    }
  },
  mutations: {
    updateConfig (state, payload) {
      state.config = payload
    },
    setConfigLoading (state, payload) {
      state.configLoading = payload
    }
  },
  actions: {
    getAll (context) {
      context.commit('setConfigLoading', true)
      const promise = Server.getServerConfig()
      promise.then((data) => {
        context.commit('updateConfig', data.body)
        context.commit('setConfigLoading', false)
      })
      promise.catch(() => {
        context.commit('setConfigLoading', false)
      })
      return promise
    }
  }
}
