import i18n from '@/i18n'
import Server from '@/services/server'

export default {
  namespaced: true,
  state: {
    listLoading: false,
    list: {},
    currentChannelId: false,
    currentChannelForbidden: true,
    currentChannel: {},
    currentChannelConductor: {},
    currentEpisodeId: false,
    currentEpisode: false,
    currentEpisodeError: false,
    pastDayConductor: false,
    forbiddenMessage: '',
    playbackReportInterval: null,
    showCalendar: false,
    loadConductorLoading: false,
    selectedDatePickerItem: null
  },
  getters: {
    listLoading: (state) => {
      return state.listLoading
    },
    list: (state) => {
      return state.list
    },
    currentChannelId: (state) => {
      return state.currentChannelId
    },
    currentChannelForbidden: (state) => {
      return state.currentChannelForbidden
    },
    currentChannel: (state) => {
      return state.currentChannel
    },
    currentChannelConductor: (state) => {
      return state.currentChannelConductor
    },
    currentEpisodeId: (state) => {
      return state.currentEpisodeId
    },
    currentEpisode: (state) => {
      return state.currentEpisode
    },
    currentEpisodeError: (state) => {
      return state.currentEpisodeError
    },
    pastDayConductor: (state) => {
      return state.pastDayConductor
    },
    forbiddenMessage: (state) => {
      return state.forbiddenMessage
    },
    playbackReportInterval: (state) => {
      return state.playbackReportInterval
    },
    showCalendar: (state) => {
      return state.showCalendar
    },
    loadConductorLoading: (state) => {
      return state.loadConductorLoading
    },
    selectedDatePickerItem: (state) => {
      return state.selectedDatePickerItem
    }
  },
  mutations: {
    setPlaybackReportInterval (state, payload) {
      state.playbackReportInterval = payload
    },
    setForbiddenMessage (state, payload) {
      state.forbiddenMessage = payload
    },
    _updateCurrentEpisodeError (state, payload) {
      state.currentEpisodeError = payload
    },
    _updatePastDayConductor (state, payload) {
      state.pastDayConductor = payload
    },
    _clearEpisode (state) {
      state.currentEpisode = false
      state.currentEpisodeId = false
      state.currentEpisodeError = false
    },
    _updateCurrentEpisodeId (state, payload) {
      state.currentEpisodeId = payload
    },
    _updateCurrentEpisode (state, payload) {
      state.currentEpisode = payload
    },
    _updateCurrentChannelId (state, payload) {
      state.currentChannelId = payload
    },
    _updateCurrentChannel (state, payload) {
      state.currentChannel = payload
    },
    _updateCurrentChannelConductor (state, payload) {
      state.currentChannelConductor = payload
    },
    _updateList (state, payload) {
      state.list = payload
    },
    _pickLastProgram (state, payload) {
      state.lastProgram = payload
    },
    clearCurrentChannel (state) {
      state.currentChannel = {}
    },
    clearAll (state) {
      clearInterval(state.playbackReportInterval)
      state.currentEpisode = false
      state.currentEpisodeError = false
      state.currentChannel = {}
    },
    clearErrors (state) {
      state.currentEpisodeError = false
    },
    setListLoading (state, payload) {
      state.listLoading = payload
    },
    setForbiddenState (state, payload) {
      state.currentChannelForbidden = payload
    },
    toggleShowCalendar (state, bool) {
      state.showCalendar = bool
    },
    setLoadConductorLoading (state, bool) {
      state.loadConductorLoading = bool
    },
    setSelectedDatePicker (state, payload) {
      state.selectedDatePickerItem = payload
    }
  },
  actions: {

    _startPlayBack: (context, payload) => {
      context.dispatch('_startReportPlaybackLoop')
      context.dispatch('mediaPlayer/startPlayback', {
        url: payload.url,
        preLoads: payload.preLoads,
        timeshift: payload.timeShift
      }, {root: true})
      context.dispatch('mediaPlayer/play', null, {root: true})
    },

    _stopReportPlaybackLoop (context) {
      clearInterval(context.state.playbackReportInterval)
    },

    _startReportPlaybackLoop (context) {
      let intervalDuration = 10
      if (context.rootState.serverConfig.config.playReportDuration && context.rootState.serverConfig.config.playReportDuration > 10) {
        intervalDuration = context.rootState.serverConfig.config.playReportDuration
      }
      clearInterval(context.state.playbackReportInterval)
      const playbackReportInterval = setInterval(() => {
        if (context.state.currentEpisodeId) {
          Server.postTvReportPlayBackByConductorId({
            channelId: context.state.currentChannelId,
            conductorId: context.state.currentEpisodeId
          }, null)
            .catch((error) => {
              context.dispatch('_errorConductorReportPlayBack', error)
            })
        } else {
          Server.postTvReportPlayBackByChannelId({
            channelId: context.state.currentChannelId
          }, null)
            .catch((error) => {
              context.dispatch('_errorConductorReportPlayBack', error)
            })
        }
      }, intervalDuration * 1000)
      context.commit('setPlaybackReportInterval', playbackReportInterval)
    },
    _errorConductorReportPlayBack (context, data) {
      switch (data.status) {
        case 403:
          context.commit('setForbiddenMessage', data.body.message)
          context.commit('_updateCurrentEpisodeError', 403)
          context.dispatch('_stopReportPlaybackLoop')
          context.dispatch('mediaPlayer/destroy', null, {root: true})
          break
        default:
          context.dispatch('_stopReportPlaybackLoop')
      }
    },
    clearEpisode (context) {
      context.commit('_clearEpisode')
    },
    getDayConductor (context, payload) {
      const promise = Server.getConductorByDate({id: context.state.currentChannelId, date: payload.day.locale('en').format('YYYY/MM/DD')})
      promise.then((response) => {
        context.commit('_updatePastDayConductor', response.body)
      })
        .catch((data) => {
          switch (data.status) {
            case 400:
              break
            default:
              context.commit('notify/error', {
                message: i18n.t('notifications.someProblem')
              }, {root: true})
          }
        })
      return promise
    },
    getEpisode (context, payload) {
      context.commit('clearErrors')
      context.commit('_updateCurrentEpisodeError', false)
      const promise = Server.getEpisode({channelId: context.state.currentChannelId, conductorId: payload})
      promise.then((response) => {
        context.commit('_updateCurrentEpisodeId', payload)
        context.commit('_updateCurrentEpisode', response.body)
        context.dispatch('_startPlayBack', response.body)
      })
        .catch((data) => {
          switch (data.status) {
            case 403:
              context.commit('_updateCurrentEpisodeError', 403)
              break
            case 404:
              context.commit('_updateCurrentEpisodeError', 404)
              break
            case 400:
              break
            default:
              context.commit('notify/error', {
                message: i18n.t('notifications.someProblem')
              }, {root: true})
          }
        })
      return promise
    },
    getChannel (context, payload) {
      // _clearChannel
      context.commit('clearErrors')
      context.commit('setForbiddenState', false)
      context.commit('setLoadConductorLoading', true)
      const promise = Promise.all([
        Server.getChannel({id: payload.id}),
        Server.getConductorByChannelId({id: payload.id})
      ]).then((response) => {
        // promise 1 - getChannel
        if (response[0].status === 200) {
          context.commit('_updateCurrentChannel', response[0].body)
          context.commit('_updateCurrentChannelId', payload.id)
          context.dispatch('_startPlayBack', response[0].body)
        }
        // promise 2 - getConductorByChannelId
        if (response[1].status === 200) {
          context.commit('_updateCurrentChannelConductor', response[1].body)
          context.commit('setLoadConductorLoading', false)
        }
      }).catch((error) => {
        context.commit('setLoadConductorLoading', false)

        switch (error.status) {
          case 403:
            context.commit('_updateCurrentEpisodeError', 403)
            context.commit('setForbiddenMessage', error.body.message)
            break
          case 400:
            break
          case 404:
            context.commit('_updateCurrentEpisodeError', 404)
            break
          default:
            context.commit('notify/error', {
              message: i18n.t('notifications.someProblem')
            }, {root: true})
        }
      })
      return promise
    },
    getOnlyChannel ({ commit, dispatch }, payload) {
      commit('clearErrors')
      commit('setForbiddenState', false)
      const promise = Server.getChannel({id: payload.id})
      promise.then((response) => {
        commit('_updateCurrentChannel', response.body)
        commit('_updateCurrentChannelId', payload.id)
        dispatch('_startPlayBack', response.body)
      })
        .catch((data) => {
          switch (data.status) {
            case 403:
              commit('_updateCurrentEpisodeError', 403)
              commit('setForbiddenMessage', data.body.message)
              break
            case 400:
              break
            default:
              commit('notify/error', {
                message: i18n.t('notifications.someProblem')
              }, {root: true})
          }
        })
      return promise
    },
    getOnlyConductor ({ commit, dispatch }, payload) {
      commit('setLoadConductorLoading', true)
      const promise = Server.getConductorByChannelId({id: payload.id})
      promise.then((response) => {
        commit('_updateCurrentChannelConductor', response.body)
        commit('setLoadConductorLoading', false)
      })
        .catch((data) => {
          commit('setLoadConductorLoading', false)
          switch (data.status) {
            case 400:
              break
            default:
              commit('notify/error', {
                message: i18n.t('notifications.someProblem')
              }, {root: true})
          }
        })
      return promise
    },
    getAll ({dispatch, commit}) {
      commit('setListLoading', true)
      const promise = Server.getChannelCategories()
      promise.then((response) => {
        commit('_updateList', response.body)
        commit('setListLoading', false)
        dispatch('tvRoute/createTVList', response.body, { root: true })
      })
      return promise
    },
    toggleShowCalendar ({ commit }, bool) {
      commit('toggleShowCalendar', bool)
    },
    setSelectedDatePicker ({commit}, payload) {
      commit('setSelectedDatePicker', payload)
    },
    clearCurrentChannel ({ commit }) {
      commit('clearCurrentChannel')
    },
    clearAll ({ commit }) {
      commit('clearAll')
    }
  }
}
