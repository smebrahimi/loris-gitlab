import i18n from '@/i18n'
import Server from '@/services/server'

export default {
  namespaced: true,
  state: {
    offset: 0,
    count: 15,
    isMore: true,
    list: [],
    featuredList: [],
    listLoading: false,
    currentProgramOtherSession: null,
    playbackReportInterval: null,
    lastProgram: null
  },
  getters: {
    isMore: (state) => {
      return state.isMore
    },
    list: (state) => {
      return state.list
    },
    featuredList: (state) => {
      return state.featuredList
    },
    listLoading: (state) => {
      return state.listLoading
    },
    currentProgramOtherSession: (state) => {
      return state.currentProgramOtherSession
    },
    playbackReportInterval: (state) => {
      return state.playbackReportInterval
    },
    lastProgram: (state) => {
      return state.lastProgram
    }
  },
  mutations: {
    _addToList (state, payload) {
      if (!payload.data.length) return
      if (payload.reload) {
        state.list = payload.data
      } else {
        // merging objects with duplicate key because used of lazy loading
        const list = [...state.list, ...payload.data]
        const object = {}
        const result = list.filter(function (item) {
          if (!object[item.date]) {
            object[item.date] = item.programs
            return true
          }
          Array.prototype.push.apply(object[item.date], item.programs)
        })
        state.list = result
      }
    },
    _pickLastProgram (state, payload) {
      // TODO: find last program from list
      state.lastProgram = payload[0].programs[0]
    },
    setListLoading (state, payload) {
      state.listLoading = payload
    },
    clearListState (state) {
      state.listLoading = false
      state.list = []
      state.isMore = true
      state.offset = 0
    },
    reloadConductor (state) {
      state.offset = 0
      state.isMore = true
    },
    setFeaturedList (state, payload) {
      const featuredList = []
      payload.forEach((item, index) => {
        const find = item.programs.filter(p => p.isFeatured === true)
        if (find) featuredList.push(...find)
      })
      state.featuredList = [...state.featuredList, ...featuredList]
    },
    updateOffset (state, payload) {
      state.offset = payload
    },
    setIsMore (state, payload) {
      state.isMore = payload
    }
  },
  actions: {
    getAll (context, payload) {
      // reload conductor list
      if (payload.reload) {
        context.commit('reloadConductor')
      }
      if (context.state.isMore) {
        const count = payload.count || context.state.count
        const data = {
          count: count,
          getFeatured: payload.getFeatured,
          type: payload.type,
          offset: context.state.offset
        }
        context.commit('updateOffset', context.state.offset + count)
        context.commit('setListLoading', true)
        const promise = Server.getPrograms(data)
        promise.then((response) => {
          if (!response.body.length) {
            context.commit('setIsMore', false)
          } else {
            if (!context.state.featuredList.length) {
              // TODO: worked on first page
              context.commit('setFeaturedList', response.body)
            }
            if (!context.state.list.length) {
              context.commit('_pickLastProgram', response.body)
            }
          }
          context.commit('_addToList', {data: response.body, reload: payload.reload})
          context.commit('setListLoading', false)
        }).catch(() => {
          context.commit('setIsMore', false)
          context.commit('notify/error', {
            message: i18n.t('notifications.someProblem')
          }, {root: true})
          context.commit('setListLoading', false)
        })
        return promise
      }
    },
    clearListState ({ commit }) {
      commit('clearListState')
    }
  }
}
