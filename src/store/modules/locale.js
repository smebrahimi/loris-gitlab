import i18n from '@/i18n'

export default {
  namespaced: true,
  state: {
    direction: 'ltr',
    alignment: 'right'
  },
  getters: {
    direction: (state) => {
      return state.direction
    },
    alignment: (state) => {
      return state.alignment
    }
  },
  mutations: {
    setLang (state, payload) {
      i18n.locale = payload
      window.localStorage.setItem('locale', payload)
    }
  },
  actions: {
    changeLang (context, payload) {
      context.commit('setLang', payload)
    }
  }
}
