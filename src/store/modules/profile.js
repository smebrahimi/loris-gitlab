import i18n from '@/i18n'
import Server from '@/services/server'

export default {
  namespaced: true,
  state: {
    profile: null,
    sendProfileChangesLoading: false,
    cachedAvatar: window.localStorage.getItem('cachedAvatar') ? JSON.parse(window.localStorage.getItem('cachedAvatar')) : null
  },
  getters: {
    profile: (state) => {
      return state.profile
    },
    sendProfileChangesLoading: (state) => {
      return state.sendProfileChangesLoading
    },
    cachedAvatar: (state) => {
      return state.cachedAvatar
    }
  },
  mutations: {
    setSendProfileChangesLoading (state, payload) {
      state.sendProfileChangesLoading = payload
    },
    cacheAvatar (state, payload) {
      window.localStorage.setItem('cachedAvatar', JSON.stringify(payload))
      state.cachedAvatar = payload
    },
    setProfileData (state, payload) {
      state.profile = payload
    }
  },
  actions: {
    getProfileInfo (context, payload) {
      const promise = Server.getUserSelf()
      promise.then((data) => {
        context.commit('auth/setMemberInfo', data.body, {root: true})
        context.commit('setProfileData', data.body)
      })
        .catch(() => {
        })
      return promise
    },
    sendProfileChanges (context, payload) {
      let cleanPayload = {}
      // TODO: none param must be ignore by server
      if (payload.fullName) {
        cleanPayload.fullName = payload.fullName
      }
      if (payload.image) {
        cleanPayload.image = payload.image
      }
      context.commit('setSendProfileChangesLoading', true)
      const promise = Server.postUser({id: context.rootState.auth.memberInfo.id}, cleanPayload)
      promise.then((data) => {
        context.commit('auth/updateMemberInfo', data.body, {root: true})
        if (payload.image) {
          context.commit('cacheAvatar', payload.image)
        }
        context.commit('notify/success', {
          message: i18n.t('notifications.accountInformation')
        }, {root: true})
        context.commit('setSendProfileChangesLoading', false)
      })
        .catch(() => {
          context.commit('setSendProfileChangesLoading', false)
        })
      return promise
    }
  }
}
