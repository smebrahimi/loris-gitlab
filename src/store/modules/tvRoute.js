import i18n from '@/i18n'
export default {
  namespaced: true,
  state: {
    isChannelListLoaded: false,
    TVList: {},
    loadingTVList: false,
    currentChannel: {
      id: 1,
      title: i18n.t('defaultChannel')
    }
  },
  getters: {
    isChannelListLoaded: (state) => {
      return state.isChannelListLoaded
    },
    TVList: (state) => {
      return state.TVList
    },
    loadingTVList: (state) => {
      return state.loadingTVList
    },
    currentChannel (state) {
      return state.currentChannel
    }
  },
  mutations: {
    setCurrentChannel (state, payload) {
      state.currentChannel = payload
    },
    setTVList (state, payload) {
      state.TVList = payload
    },
    setIsChannelListLoaded (state, bool) {
      state.isChannelListLoaded = bool
    }
  },
  actions: {
    setCurrentChannelByTitle ({state, commit}, payload) {
      switch (payload.url) {
        case 'tv':
          const id = state.TVList[payload.title]
          commit('setCurrentChannel', {slug: payload.title, id: id})
          break
      }
    },
    createTVList ({state, commit}, payload) {
      let TVList = {}
      payload.map(p => {
        TVList[p.channels[0].slug] = p.channels[0].id
      })
      commit('setTVList', TVList)
      commit('setIsChannelListLoaded', true)
    }
  }
}
