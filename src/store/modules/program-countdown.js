export default {
  namespaced: true,
  state: {
    countDownDate: 0,
    timerInterval: null,
    remainingSecond: null,
    remainingTime: {
      days: 0,
      hours: 0,
      minutes: 0,
      seconds: 0
    }
  },
  getters: {
    countDownDate: (state) => {
      return state.countDownDate
    },
    timerInterval: (state) => {
      return state.timerInterval
    },
    remainingSecond: (state) => {
      return state.remainingSecond
    },
    remainingTime: (state) => {
      return state.remainingTime
    }
  },
  mutations: {
    _updateRemainingTime (state, payload) {
      state.remainingTime = payload
    },
    clearStates (state) {
      clearInterval(state.timerInterval)
      state.countDownDate = 0
      state.timerInterval = null
      state.remainingSecond = null
      state.remainingTime = {
        days: 0,
        hours: 0,
        minutes: 0,
        seconds: 0
      }
    },
    setRemainingSecond (state, payload) {
      state.remainingSecond = payload
    },
    setTimerInterval (state, payload) {
      state.timerInterval = payload
    }
  },
  actions: {
    clearCountdown (context) {
      clearInterval(context.state.timerInterval)
    },
    startCountDown (context, payload) {
      context.commit('setRemainingSecond', payload)
      clearInterval(context.state.timerInterval)
      let i = 0
      const timerInterval = setInterval(() => {
        context.commit('setRemainingSecond', payload - i)
        context.commit('_updateRemainingTime', {
          days: Math.floor(context.state.remainingSecond / (60 * 60 * 24)),
          hours: Math.floor((context.state.remainingSecond % (60 * 60 * 24)) / (60 * 60)),
          minutes: Math.floor((context.state.remainingSecond % (60 * 60)) / (60)),
          seconds: Math.floor((context.state.remainingSecond % (60)))
        })
        if (context.state.remainingSecond < 0) {
          context.dispatch('clearCountdown')
          context.commit('clearStates')
          context.dispatch('program/callURLAccess', {}, {root: true})
        }
        i++
      }, 1000)
      context.commit('setTimerInterval', timerInterval)
    },
    clearStates ({ commit }) {
      commit('clearStates')
    }
  }
}
