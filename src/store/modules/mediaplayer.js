export default {
  namespaced: true,
  state: {
    playbackUrl: null,
    isMp4: false,
    type: null,
    isDVR: null,
    instance: null,
    preLoads: []
  },
  getters: {
    playbackUrl: (state) => {
      return state.playbackUrl
    },
    isMp4: (state) => {
      return state.isMp4
    },
    type: (state) => {
      return state.type
    },
    isDVR: (state) => {
      return state.isDVR
    },
    instance: (state) => {
      return state.instance
    },
    preLoads: (state) => {
      return state.preLoads
    }
  },
  mutations: {
    updateType (state, payload) {
      state.type = payload
      if (payload === 'mp4' || payload === 'other') {
        state.isMp4 = true
      }
    },
    updateUrl (state, payload) {
      state.playbackUrl = payload
    },
    updateDVRflag (state, payload) {
      state.isDVR = payload > 0
    },
    updatePreLoads (state, payload) {
      state.preLoads = payload
    },
    setInstance (state, payload) {
      state.instance = payload
    },
    clearStates (state) {
      state.playbackUrl = null
      state.instance = null
    },
    destroyHls (state) {
      if (state.instance && state.instance.hls) {
        state.instance.hls.destroy()
      }
    }
  },
  actions: {
    destroy (context) {
      if (context.state.instance) {
        context.state.instance.pause()
        if (context.state.instance.hls) {
          context.state.instance.hls.stopLoad()
        }
      }
    },
    play (context) {
      if (context.state.instance) {
        if (context.state.instance.hls) {
          context.state.instance.hls.startLoad()
        }
        context.state.instance.play()
      }
    },
    startPlayback (context, payload) {
      context.commit('updateType', payload.type)
      context.commit('updateUrl', payload.url)
      context.commit('updateDVRflag', payload.timeshift)
      context.commit('updatePreLoads', payload.preLoads)
    },
    clearStates ({ commit }) {
      commit('clearStates')
    },
    setInstance ({ commit }, payload) {
      commit('setInstance', payload)
    }
  }
}
