import i18n from '@/i18n'
import Server from '@/services/server'

const getToken = () => {
  let token = ''
  if (window.localStorage.getItem('token') && window.localStorage.getItem('token').length > 0) {
    token = window.localStorage.getItem('token')
  } else if (window.localStorage.getItem('user')) {
    token = JSON.parse(window.localStorage.getItem('user')).accessToken || ''
  }
  window.localStorage.setItem('token', token)
  return token
}

const getMemberInfo = () => {
  let info = null
  if (window.localStorage.getItem('memberInfo') && JSON.parse(window.localStorage.getItem('memberInfo'))) {
    info = JSON.parse(window.localStorage.getItem('memberInfo'))
  } else if (window.localStorage.getItem('user')) {
    info = JSON.parse(window.localStorage.getItem('user'))
    window.localStorage.removeItem('user')
  }
  window.localStorage.setItem('memberInfo', JSON.stringify(info))
  return info
}

export default {
  namespaced: true,
  state: {
    token: getToken(),
    isLogin: getToken() && getToken().length > 0,
    memberInfo: getMemberInfo(),
    waitForReceiveCode: false,
    step: 0,
    sendPhoneNumberLoading: false,
    sendVerificationCodeLoading: false
  },
  getters: {
    token: (state) => {
      return state.token
    },
    isLogin: (state) => {
      return state.isLogin
    },
    memberInfo: (state) => {
      return state.memberInfo
    },
    waitForReceiveCode: (state) => {
      return state.waitForReceiveCode
    },
    step: (state) => {
      return state.step
    },
    sendPhoneNumberLoading: (state) => {
      return state.sendPhoneNumberLoading
    },
    sendVerificationCodeLoading: (state) => {
      return state.sendVerificationCodeLoading
    }
  },
  mutations: {
    setSendVerificationCodeLoading (state, payload) {
      state.sendVerificationCodeLoading = payload
    },
    setSendPhoneNumberLoading (state, payload) {
      state.sendPhoneNumberLoading = payload
    },
    clearStep (state) {
      state.step = 0
    },
    login (state, payload) {
      // update token, authentication
      state.token = payload.accessToken
      state.step = 0
      state.isLogin = true
      state.memberInfo = payload
      window.localStorage.setItem('memberInfo', JSON.stringify(payload))
      window.localStorage.setItem('token', payload.accessToken)
    },
    logout (state) {
      // clear token, and authentication
      state.token = null
      state.isLogin = false
      state.memberInfo = {}
      window.localStorage.removeItem('memberInfo')
      window.localStorage.removeItem('token')
      window.localStorage.removeItem('user')
    },
    setMemberInfo (state, payload) {
      state.memberInfo = payload
      window.localStorage.setItem('memberInfo', JSON.stringify(payload))
    },
    changeFlags (state, payload) {
      state[payload.key] = payload.value
    },
    changeStep (state, payload) {
      state.step = payload
    },
    updateMemberInfo (state, payload) {
      state.memberInfo = payload
      window.localStorage.setItem('memberInfo', JSON.stringify(payload))
    }
  },
  actions: {
    sendPhoneNumber (context, payload) {
      context.commit('setSendPhoneNumberLoading', true)
      const promise = Server.postUserPhone({
        phone: payload
      })
      promise.then((data) => {
        context.commit('setMemberInfo', data.body)
        context.commit('changeStep', 2)
        context.commit('changeFlags', {
          key: 'waitForReceiveCode',
          value: true
        })
        context.commit('setSendPhoneNumberLoading', false)
      }).catch(() => {
        context.commit('notify/error', {
          message: i18n.t('notifications.loginPhoneNumber')
        }, {root: true})
        context.commit('setSendPhoneNumberLoading', false)
      })
      return promise
    },
    sendVerificationCode (context, payload) {
      context.commit('setSendVerificationCodeLoading', true)
      context.commit('changeFlags', {
        key: 'waitForReceiveCode',
        value: false
      })
      const data = {
        'code': payload,
        'phone': context.state.memberInfo.phone,
        // TODO: reza check this
        'deviceInfo': {
          manufacturer: 'Desktop',
          model: '',
          platform: 4,
          screenWidth: window.outerWidth,
          screenHeight: window.outerHeight,
          udid: `${context.state.memberInfo.phone}${Math.random()}`.split('').sort(() => 0.5 - Math.random()).join('')
        }
      }
      const promise = Server.postUserVerificationCode({id: context.state.memberInfo.id}, data)
      promise.then((data) => {
        context.commit('changeStep', 3)
        context.commit('login', data.body)
        context.commit('setSendVerificationCodeLoading', false)
        context.dispatch('profile/getProfileInfo', null, {root: true})
      }).catch((data) => {
        switch (data.status) {
          case 401: {
            context.commit('notify/error', {
              message: i18n.t('notifications.loginVerificationCode')
            }, {root: true})
            break
          }
          case 400: {
            context.commit('notify/error', {
              message: i18n.t('notifications.loginVerificationCode')
            }, {root: true})
            break
          }
        }
        context.commit('setSendVerificationCodeLoading', false)
      })
      return promise
    },
    clearStep ({ commit }) {
      commit('clearStep')
    },
    changeStep ({ commit }, payload) {
      commit('changeStep', payload)
    },
    logout ({ commit }) {
      commit('logout')
    }
  }
}
