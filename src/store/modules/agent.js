import MobileDetect from 'mobile-detect'
let md = new MobileDetect(window.navigator.userAgent)
export default {
  namespaced: true,
  state: {
    isMobile: md.mobile() || false,
    whichMobile: md.mobile(),
    whichOS: md.os()
  },
  getters: {
    isMobile: (state) => {
      return state.isMobile
    },
    whichMobile: (state) => {
      return state.whichMobile
    },
    whichOS: (state) => {
      return state.whichOS
    }
  }
}
