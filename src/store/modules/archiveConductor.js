import i18n from '@/i18n'
import lodash from 'lodash'
import Server from '@/services/server'

export default {
  namespaced: true,
  state: {
    offset: 0,
    count: 15,
    type: 3,
    listLoading: false,
    list: {},
    rawList: [],
    currentProgramOtherSession: null,
    playbackReportInterval: null,
    lastProgram: null
  },
  getters: {
    listLoading: (state) => {
      return state.listLoading
    },
    list: (state) => {
      return state.list
    },
    rawList: (state) => {
      return state.rawList
    },
    currentProgramOtherSession: (state) => {
      return state.currentProgramOtherSession
    },
    playbackReportInterval: (state) => {
      return state.playbackReportInterval
    },
    lastProgram: (state) => {
      return state.lastProgram
    }
  },
  mutations: {
    _AddToList (state, payload) {
      // TODO: must write better
      // TODO: infinite loading performance issue
      state.rawList.push(...payload)
      let newList = lodash(state.rawList).groupBy(x => x.persianDate).value()
      lodash.each(newList, (item, key) => {
        let progs = []
        item.forEach((v, i) => {
          progs.push(v.programs[0])
        })
        newList[key] = {
          programs: progs
        }
      })
      state.list = lodash(state.rawList).groupBy(x => x.persianDate).value()
    },
    _updateList (state, payload) {
      state.rawList = payload
      let newList = lodash(state.rawList).groupBy(x => x.persianDate).value()
      lodash.each(newList, (item, key) => {
        let progs = []
        item.forEach((v, i) => {
          progs.push(v.programs[0])
        })
        newList[key] = {
          programs: progs
        }
      })
      state.list = lodash(state.rawList).groupBy(x => x.persianDate).value()
    },
    _pickLastProgram (state, payload) {
      // TODO: find last program from list
      state.lastProgram = payload[0].programs[0]
    },
    setListLoading (state, payload) {
      state.listLoading = payload
    },
    clearListState (state) {
      state.listLoading = false
      state.list = {}
      state.rawList = []
      state.offset = 0
    },
    updateOffset (state, payload) {
      state.offset = payload
    }
  },
  actions: {
    searchItem (context, payload) {
      context.commit('updateOffset', 0)
      const promise = Server.getArchiveByQuery({searchText: payload})
      promise.then((response) => {
        context.commit('_updateList', response.body)
        context.commit('setListLoading', false)
      }).catch(() => {
      })
      return promise
    },
    getAll (context, payload) {
      const data = {
        count: context.state.count,
        type: context.state.type,
        offset: context.state.offset
      }
      context.commit('updateOffset', context.state.offset + context.state.count)
      if (context.state.listLoading) {
        return false
      }
      context.commit('setListLoading', true)

      const promise = Server.getArchiveList(data)
      promise.then((response) => {
        let mustPickLast = false
        if (context.state.rawList.length === 0) {
          mustPickLast = true
        }
        context.commit('_AddToList', response.body)
        // Only run first time
        if (mustPickLast) {
          context.commit('_pickLastProgram', response.body)
        }
        context.commit('setListLoading', false)
      }).catch(() => {
        context.commit('notify/error', {
          message: i18n.t('notifications.someProblem')
        }, {root: true})
        context.commit('setListLoading', false)
      })
      return promise
    },
    clearListState ({ commit }) {
      commit('clearListState')
    }
  }
}
