import Vue from 'vue'
import i18n from '@/i18n'
import Server from '@/services/server'

export default {
  namespaced: true,
  state: {
    loading: false,
    // form
    body: '',
    name: '',
    email: '',
    mobile: '',
    gRecaptcha: null
  },
  getters: {
    loading: (state) => {
      return state.loading
    },
    body: (state) => {
      return state.body
    },
    name: (state) => {
      return state.name
    },
    email: (state) => {
      return state.email
    },
    mobile: (state) => {
      return state.mobile
    },
    gRecaptcha: (state) => {
      return state.gRecaptcha
    }
  },
  actions: {
    sendFeedback ({ state, rootState, commit, dispatch }, payload) {
      state.loading = true

      const isLogin = rootState.auth.isLogin
      // TODO return promise
      const data = {
        body: state.body,
        // if user is not logged in, append `name, email, mobile` to the request body
        ...!isLogin ? {
          name: state.name,
          email: state.email,
          mobile: state.mobile,
          'g-recaptcha-response': state.gRecaptcha
        } : {}
      }
      if (isLogin) {
        const promise = Server.postFeedBack({data: data})
        promise.then((response) => {
          dispatch('_successProcess', response)
        }).catch(() => {
          dispatch('_errorProcess')
        })
      } else {
        const meta = !isLogin ? { headers: { 'g-recaptcha-response': state.gRecaptcha } } : {}
        Vue.http.post('contactUs', data, meta).then((response) => {
          dispatch('_successProcess', response)
        }, () => {
          dispatch('_errorProcess')
        })
      }
    },
    _successProcess ({state, commit}, response) {
      state.loading = false
      switch (response.status) {
        case 201:
          commit('notify/success', { message: i18n.t('message.successfulFeedback') }, { root: true })
          break
      }
    },
    _errorProcess ({state, commit}) {
      commit('notify/error', { message: i18n.t('message.failedFeedback') }, { root: true })
      state.loading = false
    }
  },
  mutations: {
    updateProperty (state, { key, value }) {
      state[key] = value
    }
  }
}
