import Vue from 'vue'
import i18n from '@/i18n'
import Server from '@/services/server'

export default {
  namespaced: true,
  state: {
    OTP: null,
    packages: null,
    packagesLoading: false,
    programPackagesLoading: false,
    buyPackageStep: null,
    buyPackageLoading: false,
    verificationLoading: false,
    currentPackage: null,
    programPackages: null,
    packageID: null
  },
  getters: {
    OTP: (state) => {
      return state.OTP
    },
    packages: (state) => {
      return state.packages
    },
    packagesLoading: (state) => {
      return state.packagesLoading
    },
    programPackagesLoading: (state) => {
      return state.programPackagesLoading
    },
    buyPackageStep: (state) => {
      return state.buyPackageStep
    },
    buyPackageLoading: (state) => {
      return state.buyPackageLoading
    },
    verificationLoading: (state) => {
      return state.verificationLoading
    },
    currentPackage: (state) => {
      return state.currentPackage
    },
    programPackages: (state) => {
      return state.programPackages
    },
    packageID: (state) => {
      return state.packageID
    }
  },
  mutations: {
    updateBuyPackageLoading (state) {
      state.buyPackageLoading = !state.buyPackageLoading
    },
    updateSendVerificationLoading (state) {
      state.verificationLoading = !state.verificationLoading
    },
    setBuyPackageStep (state, payload) {
      state.buyPackageStep = payload
    },
    clearOTPState (state) {
      state.buyPackageStep = null
      state.currentPackage = null
      state.programPackages = null
    },
    updateCurrentPackage (state, payload) {
      state.currentPackage = payload
    },
    setPackages (state, payload) {
      state.packages = payload
    },
    setProgramPackages (state, payload) {
      state.programPackages = payload
    },
    setPackageID (state, payload) {
      state.packageID = payload
    },
    setPackagesLoading (state, payload) {
      state.packagesLoading = payload
    },
    setOtp (state, payload) {
      state.OTP = payload
    },
    setProgramPackagesLoading (state, bool) {
      state.programPackagesLoading = bool
    }
  },
  actions: {
    getAll (context) {
      context.commit('setPackagesLoading', true)
      const promise = Server.getPackages({id: context.rootState.auth.memberInfo.id})
      promise
        .then((data) => {
          context.commit('setPackages', data.body)
          context.commit('setPackagesLoading', false)
        })
        .catch(() => {
          context.commit('setPackagesLoading', false)
        })
      return promise
    },
    getProgramPackages ({ commit, rootState }, payload) {
      commit('setProgramPackagesLoading', true)
      const promise = Server.getPackageByProgramId({memberId: rootState.auth.memberInfo.id, programId: payload})
      promise
        .then((data) => {
          commit('setProgramPackages', data.body)
          commit('setProgramPackagesLoading', false)
        }, () => {
          commit('setProgramPackagesLoading', false)
        })
      return promise
    },
    sendVerificationCode (context, payload) {
      context.commit('updateSendVerificationLoading')
      const path = (context.state.currentPackage.redirectTo) ? context.state.currentPackage.redirectTo.slice(1) : `users/${context.rootState.auth.memberInfo.id}/packages`
      Vue.http.post(path, {
        'otpPin': payload
      }).then((data) => {
        context.commit('updateSendVerificationLoading')
        if (data.status === 200) {
          context.commit('setBuyPackageStep', 4)
          if (context.rootState.program.currentProgramID) {
            context.dispatch('program/reloadCurrentProgram', null, {root: true})
          }
          if (context.rootState.archive.currentProgramID) {
            context.dispatch('archive/reloadCurrentProgram', null, {root: true})
          }
        }
      }, (data) => {
        context.commit('updateSendVerificationLoading')
        switch (data.status) {
          case 400:
            context.commit('notify/error', {
              message: data.body.message
            }, {root: true})
            context.commit('setBuyPackageStep', 2)
            break
          case 410:
            context.commit('setBuyPackageStep', 3)
            break
          default:
            const error = new Error('Client Error: Submit OTP request, responded with wrong status code')
            throw (error)
        }
      })
    },
    buyPackage (context, payload) {
      context.commit('updateBuyPackageLoading')
      const promise = Server.postPackage({memberId: context.rootState.auth.memberInfo.id}, {packageId: payload})
      promise.then((data) => {
        context.commit('updateBuyPackageLoading')
        context.commit('updateCurrentPackage', data.body)
        switch (data.status) {
          case 200:
            if (data.body.status === 2) {
              context.commit('setBuyPackageStep', 4)
              if (context.rootState.program.currentProgramID) {
                context.dispatch('program/reloadCurrentProgram', null, {root: true})
              }
              if (context.rootState.archive.currentProgramID) {
                context.dispatch('archive/reloadCurrentProgram', null, {root: true})
              }
            } else if (data.body.status === 3) {
              // show Unsupported Phone Number screen
              context.commit('setBuyPackageStep', 5)
            }
            break
          case 202:
            // Bank gateway
            if (data.body.gateway && data.body.gateway === 'Bank' && data.body.redirectTo) {
              window.location.href = data.body.redirectTo
            } else {
              // show OTP input
              context.commit('setBuyPackageStep', 2)
            }
            break
          default:
            let error = new Error('Client Error: Buy package request get wrong status code')
            throw (error)
        }
      })
        .catch((data) => {
          context.commit('updateBuyPackageLoading')
          if (data && data.status === 412) {
            context.commit('setBuyPackageStep', 6)
            context.commit('updateCurrentPackage', data.body)
          } else {
            context.commit('notify/error', {
              message: i18n.t('notifications.someProblem')
            }, {root: true})
          }
        })
      return promise
    },
    clearOTPState ({ commit }) {
      commit('clearOTPState')
    },
    setPackageID ({ commit }, payload) {
      commit('setPackageID', payload)
    },
    setBuyPackageStep ({ commit }, payload) {
      commit('setBuyPackageStep', payload)
    }
  }
}
