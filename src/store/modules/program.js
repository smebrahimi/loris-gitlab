import Vue from 'vue'
import i18n from '@/i18n'
import Server from '@/services/server'

export default {
  namespaced: true,
  state: {
    currentProgramID: null,
    currentProgramData: {},
    currentProgramOtherSession: null,
    playbackReportInterval: null,
    _checkPlayAbilityTimer: null,
    isLocked: null,
    isMissing: null,
    isFinished: null,
    isForbidden: null,
    forbiddenMessage: '',
    reportProblemLoading: false
  },
  getters: {
    currentProgramID: (state) => {
      return state.currentProgramID
    },
    currentProgramData: (state) => {
      return state.currentProgramData
    },
    currentProgramOtherSession: (state) => {
      return state.currentProgramOtherSession
    },
    playbackReportInterval: (state) => {
      return state.playbackReportInterval
    },
    isLocked: (state) => {
      return state.isLocked
    },
    isMissing: (state) => {
      return state.isMissing
    },
    isForbidden: (state) => {
      return state.isForbidden
    },
    isFinished: (state) => {
      return state.isFinished
    },
    forbiddenMessage: (state) => {
      return state.forbiddenMessage
    },
    reportProblemLoading: (state) => {
      return state.reportProblemLoading
    }
  },
  mutations: {
    setForbiddenMessage (state, payload) {
      state.forbiddenMessage = payload
    },
    setIsForbidden (state, payload) {
      state.isForbidden = payload
    },
    setFinished (state) {
      state.isFinished = true
    },
    setLocked (state) {
      state.isLocked = true
    },
    resetLocked (state) {
      state.isLocked = false
    },
    _updateCurrentProgram (state, payload) {
      state.currentProgramData = payload

      document.title = payload.title
    },
    setCurrentProgramID (state, payload) {
      state.currentProgramID = payload
    },
    _setOtherSession (state, payload) {
      state.currentProgramOtherSession = payload
    },
    _clearOtherSession (state) {
      state.currentProgramOtherSession = null
    },
    clearStates (state) {
      clearInterval(state.playbackReportInterval)
      clearTimeout(state._checkPlayAbilityTimer)
      state.currentProgramID = null
      state.currentProgramData = {}
      state.currentProgramOtherSession = null
      state.playbackReportInterval = null
      state.isLocked = null
      state.isMissing = null
      state.isFinished = null
    },
    setIsMissing (state, payload) {
      state.isMissing = payload
    },
    setIsLocked (state, payload) {
      state.isLocked = payload
    },
    setIsFinished (state, payload) {
      state.isFinished = payload
    },
    setPlaybackReportInterval (state, payload) {
      state.playbackReportInterval = payload
    },
    setCheckPlayAbilityTimer (state, payload) {
      state._checkPlayAbilityTimer = payload
    },
    setReportProblemLoading (state, bool) {
      state.reportProblemLoading = bool
    }
  },
  actions: {
    getById (context, payload) {
      const promise = Server.getProgramById({
        id: payload
      })
      promise.then((response) => {
        context.commit('_updateCurrentProgram', response.body)
        // Prevent check program playability if user is not login
        if (context.rootState.auth.isLogin) {
          context.dispatch('checkPlayability')
        }
      }).catch((data) => {
        switch (data.status) {
          case 404:
            context.commit('setIsMissing', true)
            break
          case 0:
            context.commit('setIsMissing', true)
            break
          default:
            context.commit('notify/error', {
              message: i18n.t('notifications.someProblem')
            }, {root: true})
        }
      })
      return promise
    },
    reloadCurrentProgram (context) {
      context.dispatch('getById', context.state.currentProgramID)
    },
    killOtherSession (context) {
      context.dispatch('callURLAccess', context.state.currentProgramOtherSession[0])
    },

    _startPlayBack: (context, payload) => {
      context.dispatch('_startReportPlaybackLoop')
      context.dispatch('mediaPlayer/startPlayback', {url: payload.url, preLoads: payload.preLoads, timeshift: payload.timeShift}, {root: true})
      context.dispatch('mediaPlayer/play', null, {root: true})
    },

    _stopReportPlaybackLoop (context) {
      clearInterval(context.state.playbackReportInterval)
    },

    _startReportPlaybackLoop (context) {
      let intervalDuration = 10
      if (context.rootState.serverConfig.config.playReportDuration && context.rootState.serverConfig.config.playReportDuration > 10) {
        intervalDuration = context.rootState.serverConfig.config.playReportDuration
      }
      clearInterval(context.state.playbackReportInterval)
      const playbackReportInterval = setInterval(() => {
        const promise = Server.postProgramReportPlayBack({
          id: context.state.currentProgramData.id
        }, null)
        promise.then((data) => {
          // console.log('reportPlayback is happy :)')
        })
          .catch((data) => {
            switch (data.status) {
              case 401:
                context.commit('_setOtherSession', data.body.sessions)
                context.dispatch('_stopReportPlaybackLoop')
                context.dispatch('mediaPlayer/destroy', null, {root: true})
                break
              case 403:
                // console.log('TODO:  Show promotions return to playability check')
                context.commit('clearStates')
                context.dispatch('_stopReportPlaybackLoop')
                context.commit('setFinished')
                break
              default:
                context.dispatch('_stopReportPlaybackLoop')
                let error = new Error('Client Error : reportPlayback request get wrong status code')
                throw (error)
            }
          })
      }, intervalDuration * 1000)
      context.commit('setPlaybackReportInterval', playbackReportInterval)
    },

    callURLAccess (context, payload = {id: null}) {
      context.commit('setIsForbidden', false)
      Vue.http.get(`programs/${context.state.currentProgramData.id}/urlAccess`, {
        params: payload.id ? {killSessionId: payload.id} : {}
      })
        .then((data) => {
          context.commit('_clearOtherSession')
          context.dispatch('_startPlayBack', data.body)
        })
        .catch((data) => {
          switch (data.status) {
            case 401:
              context.commit('_setOtherSession', data.body.sessions)
              break
            case 402:
              // console.log('TODO:  Show demo if exists! Does not implemented')
              break
            case 403:
              context.commit('setIsForbidden', true)
              context.commit('setForbiddenMessage', data.body.message)
              break
            case 423:
              // TODO: be careful about this line, infinite loop warn
              context.commit('setLocked')
              context.dispatch('_startCheckPlayAbilityTimer', {
                newStartAt: data.body.startAt
              })
              break
            default:
          }
        })
    },

    _startCheckPlayAbilityTimer (context, payload) {
      if (context.state._checkPlayAbilityTimer) {
        clearTimeout(context.state._checkPlayAbilityTimer)
      }
      if (payload.newStartAt && payload.newStartAt !== context.state.currentProgramData.startAt) {
        context.commit('resetLocked')
        context.dispatch('getById', context.state.currentProgramID)
      } else {
        const checkPlayAbilityTimer = setTimeout(() => {
          context.commit('resetLocked')
          context.dispatch('getById', context.state.currentProgramID)
          // TODO: move this to config
        }, 20000)
        context.commit('setCheckPlayAbilityTimer', checkPlayAbilityTimer)
      }
    },

    _startCountdown (context) {
      context.dispatch('programCountdown/startCountDown', context.state.currentProgramData.remainingToStart, {root: true})
    },

    checkPlayability (context) {
      const program = context.state.currentProgramData
      if (program.lock === true) {
        // TODO: must complete
        return false
      }

      if (Vue._.isNumber(program.type)) {
        if (program.isLive === true) {
          context.dispatch('callURLAccess')
        } else if (program.isLive === false) {
          context.dispatch('_startCountdown')
        } else {
          let error = new Error('Client Error : Program isLive property has wrong value!')
          throw (error)
        }
      }
    },
    sendReportProblem ({ commit, rootGetters }, payload) {
      commit('setReportProblemLoading', true)
      const programId = rootGetters['program/currentProgramData'].id || rootGetters['archive/currentProgramData'].id
      const promise = Server.postProgramIssue({id: programId}, payload)
      promise.then((response) => {
        commit('setReportProblemLoading', false)
        switch (response.status) {
          case 201:
            commit('notify/success', {
              message: response.message ? response.message : i18n.t('notifications.messageRecorded')
            }, {root: true})
            break
          default:
            let error = new Error('Client Error : Please try Again')
            throw (error)
        }
      })
        .catch(() => {
          commit('setReportProblemLoading', false)
          commit('notify/error', {
            message: i18n.t('notifications.someProblem')
          }, {root: true})
        })
    },
    clearStates ({commit}) {
      commit('clearStates')
    },
    setCurrentProgramID ({commit}, payload) {
      commit('setCurrentProgramID', payload)
    }
  }
}
