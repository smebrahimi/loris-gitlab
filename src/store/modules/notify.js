export default {
  namespaced: true,
  state: {
    message: null,
    type: null,
    count: 0
  },
  getters: {
    message: (state) => {
      return state.message
    },
    type: (state) => {
      return state.type
    },
    count: (state) => {
      return state.count
    }
  },
  mutations: {
    success (state, payload = {action: '', message: ''}) {
      state.message = payload.message
      state.type = 'success'
      state.count = state.count + 1
    },
    error (state, payload = {action: '', message: ''}) {
      state.message = payload.message
      state.type = 'error'
      state.count = state.count + 1
    }
  },
  actions: {
    show ({ commit }, payload) {
      if (!payload.type || !payload.message) {
        return
      }
      commit(`${payload.type}`, {message: payload.message})
    },
    success ({ commit }, payload) {
      commit('success', payload)
    },
    error ({ commit }, payload) {
      commit('error', payload)
    }
  }
}
