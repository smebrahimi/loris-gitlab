## Apr 16, 18
  - Hotfix conductor programs list page size increased

## Mar 18, 18
  - Add destroyHls mutation to mediaPlayer module
  - Hotfix: program, add isFinished to clearState and fix clearState calling order inside playBackUrk 403 error response

## Mar 17, 18
  - check playReportDuration before use in setInterval

## Mar 14, 18
  - clear _checkPlayAbilityTimer timer on program clearState
  - call _stopReportPlaybackLoop on reportPlayBack get undefined error

## Mar 13, 18
  - disable check startAt on urlAccess 423 response
  - handle 500 (0 status code) error in get program api
  - handle 400 (bad request) on get channel data
  - add clearListState mutation to conductor

## Mar 12, 18
  - Handle callURLAccess 423 response

## Mar 8, 18
  - Refactored contact-us module to support both guest & user mode
