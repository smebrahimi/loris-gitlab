import Vue from 'vue'
import store from '@/store'
import Router from 'vue-router'
import i18n from '@/i18n'
import Login from '@main/pages/Login'
import Logout from '@main/pages/Logout'
import Kitchen from '@main/pages/Kitchen'
import Profile from '@main/pages/Profile'
import Notification from '@main/pages/Notification'
import Error404 from '@main/pages/Error404'
import Program from '@main/pages/Program'
import Landing from '@main/pages/Landing'
import Package from '@main/pages/Package'
import About from '@main/pages/AboutUs'
import Contact from '@main/pages/ContactUs'
import TV from '@main/pages/TV'
import Archive from '@main/pages/Archive'
import ArchiveItem from '@main/pages/ArchiveItem'

Vue.use(Router)
let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '*',
      name: '404',
      component: Error404,
      meta: {
        title: i18n.t('header.error404')
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: {
        title: i18n.t('login')
      }
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
      meta: {
        title: i18n.t('profile'),
        auth: true
      }
    },
    {
      path: '/logout',
      name: 'Logout',
      component: Logout,
      meta: {
        title: i18n.t('logout'),
        showProgressBar: false,
        auth: true
      }
    },
    {
      path: '/kitchen',
      name: 'Kitchen',
      component: Kitchen,
      meta: {
        title: 'Kitchen'
      }
    },
    {
      // TODO: add program id to this route
      path: '/program',
      name: 'Root',
      alias: '/',
      component: Landing,
      meta: {
        title: i18n.t('program')
      }
    },
    {
      path: '/program/:id',
      name: 'Program',
      component: Program,
      meta: {
        title: i18n.t('program')
      }
    },
    {
      path: '/notification',
      name: 'Notification',
      component: Notification,
      meta: {
        title: i18n.t('notification'),
        auth: true
      }
    },
    {
      path: '/tv',
      name: 'TV',
      component: TV,
      meta: {
        title: i18n.t('tv')
      }
    },
    {
      path: '/tv/:title_slug',
      name: 'TV',
      component: TV,
      meta: {
        title: i18n.t('tv')
      }
    },
    {
      path: '/package',
      name: 'Package',
      component: Package,
      meta: {
        title: i18n.t('userPackage'),
        auth: true
      }
    },
    {
      path: '/archive',
      name: 'Archive',
      component: Archive,
      meta: {
        title: i18n.t('archive')
      }
    },
    {
      path: '/archive/:id/:title_slug?',
      name: 'Archive',
      component: ArchiveItem,
      meta: {
        title: i18n.t('archive')
      }
    },
    {
      path: '/about-us',
      name: 'About',
      component: About,
      meta: {
        title: i18n.t('aboutUs')
      }
    },
    {
      path: '/contact-us',
      name: 'Contact',
      component: Contact,
      meta: {
        title: i18n.t('contactUs')
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if ($('video')[0]) {
    $('.video-js-box').hide()
    $('video')[0].pause()
    $('video')[0].setAttribute('src', '')
    $('video')[0].src = ['']
  }
  document.title = to.meta.title
  if (to.meta.auth && !store.state.auth.isLogin) {
    next('/login')
  } else if (to.name === 'Login' && store.state.auth.isLogin) {
    next('/')
  } else {
    next()
  }
})
export default router
