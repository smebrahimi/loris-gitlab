# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v0.7.30] - 2019-02-11
### Fixed
- Remove all persian text from components files
- Revise & redesign layout of components
- Fix layout of program card & timeline upgrade
### Added
- Upgrade store

## [v0.7.29] 2019-01-23
## Fixed
- Added phone validation for contact us page
- IE 10 & 11, timeline not center in the program & archive page
- Reporter, supported responsive ui for four reporters
- rtl direction for input phone in the buy package
- when opened modal, container move bit
- Edit padding when pluti line title in the program details
- Selected day on calendar when conductor is empty
- Removed old files & extra code

## [v0.7.28] - 2019-01-22
### Fixed
- Changed version copy right
- space between footer with contact us

## [v0.7.27] - 2019-01-16
### Added
- Logo samandehi

## [v0.7.26] - 2019-01-15
### Fixed
- Added margin bottom for archive page (space footer)
- Hotfix infinite scroll for program page
- ProgramCard thumbnail effect in Firefox
- active tab after refresh on program page

## [v0.7.24] - 2019-01-23
## Fixed
- Added version to last filename in the index file
 
## [v0.7.23] - 2019-01-23
## Fixed
- If the user has granted access not run push notification again

## [v0.7.22] - 2019-01-12
### Fixed 
- Changed platform to clientPlatform in api call program for backend 

## [v0.7.21] - 2019-01-12
### Added
- Program Reporters

## [v0.7.20] - 2019-01-07
### Fixed
- Route tv on mobile

## [v0.7.19] - 2019-01-05
### Fixed
- tv routes, switch from title to slug

## [v0.7.18] - 2019-01-01
### Fixed
- Checked user authorization & update user after subscribe by push notification 
- Dynamically route tv

## [v0.7.17] - 2018-12-31
## Fixed 
- Added unsubscribe to push notification

## [v0.7.16] - 2018-12-31
### Fixed
- Hotfix no result on tv page

## [v0.7.15] 2018-12-30
### Added
- push notification

## [v0.7.14]
### Added
- gzip command to scripts

## [v0.7.13]
### Fixed
- Hotfix repeat programs list label

## [v0.7.12] - 2018-12-18
### Fixed
- changed program type id tv 

## [v0.7.11] - 2018-12-18
### Fixed
- Hotfix active tabs by config server

## [v0.7.10] 
### Fixed
- Android chrome 71 fullscreen bug

## [v0.7.9] - 2018-12-09
### Fixed
- Break line in the description of archive page
- Hotfix improve UI vs icon in the program card
### Changed
- Buy Package for rightel show new message in the modal (Changed procedure for rightel)
- Disable recommended section
- Split time of program cards in timeline (pages: archive and programs)
- Enable webpack gzip on build

## [v0.7.8] - 2018-11-27
### Fixed
- Hotfix removed pipeline and used of dash

## [v0.7.7] - 2018-11-27
### Fixed
- Hotfix removed .vs from program card

## [v0.7.6] - 2018-11-27
### Fixed
- Player bug on first page load
- Improve responsive program card

## [v0.7.5] - 2018-11-21
### Added
- Featured programs, show slider in the landing page when program is featured
- Recommended videos in the program page
- Added mp4 format instead of hls
- Spell check lint
- Added slug name url into tv & archive page
### Changed
- New changelog [convention](https://keepachangelog.com/en/0.3.0/)
- Upgrade timeline UI in the archive & TV page

## [v0.7.4] - 2018-10-28
### Added
- Added next prev rel of link tag to the archive page
- Added new meta tag & link tag into head of index.html file
- Added vue lint into the project

### Fixed
- Resolved errors of vue lint by auto (--fix) & manually
- Fixed style of reload button in the program page
- Fixed minor UI in the program page
- Fixed minor UI in the profile page
- Fixed duplicate header in the program page
- Fixed location of overstrap file into main.scss file
- Fixed footer responsive mobile - margin not good

### Removed
- Removed read more of description link from the program page

## [v0.7.3] - oct 28, 18
  - Hotfie remove request to https://live-api.anten.ir/

## [v0.7.2] - oct 24, 18
  - Hotfix Archive page title
  - Fix tv page responsive bug
  - Add app link in footer

## [v0.7.1] - oct 17, 18
  - Hotfix player memory problem
  - Hotfix player load rendering blink issue

## [v0.7.0] - oct 15, 18
  - New UI

## [v0.6.15] - sep 19, 18
  - Hotfix 404 error page title issue
  - Hotfix load user profile after login

## [v0.6.14] - sep 15, 18
  - Tv page channels UI issue in mobile (change owl carousel items count)
  - Disable zoom in All mobile device (Android Chrome)
  - Remove About Us/Contact Us menu from header
  - Change program route title in header

## [v0.6.13] - Sep 12, 18
  - Add thumbnails to TV
  - Disable zoom in mobile devices

## [v0.6.12] - Sep 8, 18
  - Hotfix tv player background issue

## [v0.6.11] - Sep 3, 18
  - Read vast config from server config api
  - Change TV background
  - Fix tv conductor performance issue

## [v0.6.10] - Aug 27, 18
  - Show announce to rightel users

## [v0.6.9] - Aug 27, 18
  - Change VAST tracker url

## [v0.6.8] - Aug 25, 18
  - Implement report playback to TV

## [v0.6.7] - Aug 25, 18
  - Implement two week catchup tv

## [v0.6.7] - Aug 19, 21
  - Add report playback request to TV and episodes

## [v0.6.6] - Aug 19, 18
  - Implement vast tracker

## [v0.6.5] - Aug 16, 18
  - Fix 403 error message ui issue

## [v0.6.4] - Aug 15, 18
  - Hotfix IOS VAST

## [v0.6.3] - Aug 15, 18
  - Add VAST to live programes
  - Currently VAST is disable on IOS

## [v0.6.2] - Aug 8, 18
  - Handle 403 error

## [v0.6.1] - Aug 1, 18
  - Remove worldcup background

## [v0.6.0] - Jul 20, 18
  - player@0f06530
  - add tv catchup
  - add DVR functionality to TV
  - add DVR functionality to anten programs

## [v0.5.12] - Jul 11, 18
  - Make pre git linting enable (enable stylelint)

## [v0.5.11] - Jul 11, 18
  - Make pre git linting enable

## [v0.5.10] - Jun 19, 18
  - Fix ios stream loading issue
  - player@5fa335e

## [v0.5.9] - Jun 17, 18
  - Fix ios stop hls error
  - Fix ios tv Auto play

## [v0.5.8] - Jun 17, 18
  - Fix firefox multiple scroll bug

## [v0.5.7] - Jun 17, 18
  - Resolve eslint errors

## [v0.5.6] - Jun 17, 18
  - Fix packages redundant request
  - Fix upload profile data bug with avatar
  - Fix profile upload button loading state
  - Add load user profile data on page load and user is logged in

## [v0.5.5] - Jun 15, 18
  - Fix archive responsive view
  - Fix Header responsive view
  - Fix SideNav responsive view

## [v0.5.4] - Jun 14, 18
  - Fix price text in buy package modal

## [v0.5.3] - Jun 14, 18
  - Hotfix UI

## [v0.5.2] - Jun 14, 18
  - Revert tv routes and nav links
  - Update live and archive background images

## [v0.5.0] - Jun 13, 18
  - Fix archive packages
  - Some changes on archive list cards
  - Update google anal... to UA-97890887-11
  - Support preLoads (image ads before player)

## [v0.4.5] - Jun 13, 18
  - Fix autoplay issue after update player

## [v0.4.4] - Jun 13, 18
  - Hotfix remove tv icon in mobile view

## [v0.4.3] - Jun 13, 18
  - Hide tv page and routes

## [v0.4.2] - Jun 12, 18
  - Fix Tv issue with kill list
  - Merge lock program branch

## [v0.4.1] - Jun 11, 18
  - Tv page ui fix

## [v0.4.0] - Jun 9, 18
  - Archive page
  - New player@videojs@6.0.0

## [v0.3.6] - Jun 11, 18
  - Change logo to old logo

## [v0.3.5] - Apr 22, 18
  - Fixed conductor rtl

## [v0.3.4] - Apr 22, 18
  - Fixed conductor *is live indocator* to be shown depending on status & type of the program

## [v0.3.3] - Apr 16, 18
  - Added *Shima* logo

## [v0.3.2] - Apr 16, 18
  - Hotfix i18n

## [v0.3.1] - Apr 11, 18
  - Replaced Favicon with new version

## [v0.3.0] - Apr 9, 18
  - Hotfix: Make conductor upper title invisible when it's unavailable
  - Small UI issues fixed

## [v0.2.13] - Apr 4, 18
  - Hotfix player remove unwanted google material font resource

## [v0.2.12] - Mar 26, 18
  - Hotfix autoswitch HLS quality (adaptive bitrate set to **auto**)

## [v0.2.11] - Mar 26, 18
  - Hotfix Kill redundant HLS streams in TV

## [v0.2.10] - Mar 19, 18
  - Hotfix urlAccess 423 reload countdown timer

## [v0.2.9] - Mar 19, 18
  - Hotfix TV channel keep view mode
  - Hotfix TV channel prevent load unwanted stream
  - Hotfix TV program finished state

## [v0.2.8] - Mar 18, 18
  - Hotfix TV channel switch
  - Minor style changes in tv

## [v0.2.7] - Mar 18, 18
  - Hotfix Exclude some routes from 401 unsetting token
  - Hotfix TV URL assignment
  - Fixed RTL texts

## [v0.2.6] - Mar 18, 18
  - Hotfix conductor and program page title rtl text
  - Hotfix reportPlayBack check received config before set on setInterval
  - Hotfix conductor upper title
  - Hotfix program 404
  - Hotfix TV channel
  - Hotfix Handle 401 HTTP status code
  - Hotfix tex-rtl in conductor uppertitle

## [v0.2.5] - Mar 13, 18
  - Hotfix conductor loading after user back from other pages
  - Hotfix footer logo must be link
  - Replace appbar icon with a favicon image and removed redundant image file

## [v0.2.4] - Mar 13, 18
  - Added `embed` computed to mixins

## [v0.2.3] - Mar 13, 18
  - Add green circle aside live text in conductor

## [v0.2.2] - Mar 13, 18
  - Remove `user` from localStorage on logout

## [v0.2.1] - Mar 13, 18
  - Fixed footer bottom margin
  - Added old anten users migration script to auth module

## [v0.2.0] - Mar 13, 18
  - Fix 404 error page style
  - Added version number to the footer

## Mar 12, 18
  - Added `vue-analytics`
  - Fixed conductor logo container aspect ratio bug in iOS
  - Fix login catch in safari and handle errors related to login

## Mar 11, 18
  - Fixed conductor circle shape style
  - Fixed `vjs-loading-spinner` clipped circle style
