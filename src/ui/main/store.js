import store from '@/store'
store.registerModule('layout', {
  namespaced: true,
  state: {
    showLogoutModal: false,
    showLoginModal: false,
    showSidebar: false,
    showOTPModal: false,
    showReportModal: false,
    showAppbar: (!(JSON.parse(window.localStorage.getItem('visitedAppbar'))) && (store.state.agent.whichOS === 'iOS' || store.state.agent.whichOS === 'AndroidOS')),
    orientation: window.innerWidth > window.innerHeight ? 'Landscape' : 'Portrait',
    responsiveSize: window.innerWidth
  },
  getters: {
    isShowLogoutModal: (state) => {
      return state.showLogoutModal
    },
    isShowLoginModal: (state) => {
      return state.showLoginModal
    },
    showSidebar: (state) => {
      return state.showSidebar
    },
    showOTPModal: (state) => {
      return state.showOTPModal
    },
    isShowReportModal: (state) => {
      return state.showReportModal
    },
    showAppbar: (state) => {
      return state.showAppbar
    },
    orientation: (state) => {
      return state.orientation
    },
    responsiveSize: (state) => {
      return state.responsiveSize
    }
  },
  mutations: {
    resetAppbarVisibility (state) {
      window.localStorage.removeItem('visitedAppbar')
      state.showAppbar = (!(JSON.parse(window.localStorage.getItem('visitedAppbar'))) && (store.state.agent.whichOS === 'iOS' || store.state.agent.whichOS === 'AndroidOS'))
    },
    setUpdateLayoutData (state) {
      state.orientation = window.innerWidth > window.innerHeight ? 'Landscape' : 'Portrait'
    },
    setUpdateResponsiveSize (state) {
      state.responsiveSize = window.innerWidth
    },
    setClear (state) {
      state.showLogoutModal = false
      state.showSidebar = false
      state.showLoginModal = false
    },
    showLogoutModal (state) {
      state.showLogoutModal = true
    },
    setHideLogoutModal (state) {
      state.showLogoutModal = false
    },
    showLoginModal (state) {
      state.showLoginModal = true
    },
    hideLoginModal (state) {
      state.showLoginModal = false
    },
    showOTPModal (state) {
      state.showOTPModal = true
    },
    hideOTPModal (state) {
      state.showOTPModal = false
    },
    toggleSidebar (state) {
      state.showSidebar = !state.showSidebar
    },
    setHideSidebar (state) {
      state.showSidebar = false
    },
    hideAppbar (state) {
      window.localStorage.setItem('visitedAppbar', true)
      state.showAppbar = false
    },
    showReportModal (state) {
      state.showReportModal = true
    },
    hideReportModal (state) {
      state.showReportModal = false
    }
  },
  actions: {
    showLoginModal ({ commit }) {
      commit('showLoginModal')
    },
    hideLoginModal ({ commit }) {
      commit('hideLoginModal')
    },
    showLogoutModal ({ commit }) {
      commit('showLogoutModal')
    },
    showReportModal ({ commit }) {
      commit('showReportModal')
    },
    hideReportModal ({ commit }) {
      commit('hideReportModal')
    },
    updateLayoutData ({commit}) {
      commit('setUpdateLayoutData')
    },
    updateResponsiveSize ({ commit }) {
      commit('setUpdateResponsiveSize')
    },
    clear ({ commit }) {
      commit('setClear')
    },
    hideLogoutModal ({ commit }) {
      commit('setHideLogoutModal')
    },
    hideSidebar ({ commit }) {
      commit('setHideSidebar')
    },
    resetAppbarVisibility ({ commit }) {
      commit('resetAppbarVisibility')
    },
    hideAppbar ({ commit }) {
      commit('hideAppbar')
    },
    toggleSidebar ({commit}) {
      commit('toggleSidebar')
    },
    showOTPModal ({ commit }) {
      commit('showOTPModal')
    },
    hideOTPModal ({ commit }) {
      commit('hideOTPModal')
    }
  }
})
