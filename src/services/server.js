import Vue from 'vue'
import VueResource from 'vue-resource'
import Api from './api'
import store from '@/store/index'
import router from '@/ui/main/router'

Vue.use(VueResource)

const allApi = {
  ...Api
}
// Note: Before Request
Vue.http.interceptors.push((request, next) => {
  const token = store.state.auth.token
  if (store.state.auth.isLogin) {
    request.headers.set('X-CSRF-TOKEN', 'TOKEN')
    request.headers.set('Authorization', `${token}`)
  }
  next((response) => {
    if (response.status === 401) {
      const requestUrl = request.url

      /*
        following URLs are OK to respond 401 (user should not be logged out)

        'users/{id}/sessions'
        'programs/{id}/urlAccess'
        'programs/{id}/reportPlayback'
      */

      // todo: do this in correct way
      if (requestUrl.indexOf('/sessions') === -1 && requestUrl.indexOf('/urlAccess') === -1 && requestUrl.indexOf('/reportPlayback') === -1) {
        store.commit('auth/logout')

        router.push({ name: 'Login' })
      }
    }
  })
})

const Server = Vue.resource('', {}, allApi)
export default Server
