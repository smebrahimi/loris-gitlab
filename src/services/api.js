import Config from '@/config/config.js'

export const BaseUrl = Config.apiUrl

export default {
  // MISC
  getServerConfig: { method: 'GET', url: BaseUrl + 'apps/config' },
  // Auth
  postUserPhone: {method: 'POST', url: BaseUrl + 'users/{phone}'},
  postUserVerificationCode: {method: 'POST', url: BaseUrl + 'users/{id}/sessions/{data}'},
  // Profile
  getUserSelf: {method: 'GET', url: BaseUrl + 'users/self'},
  postUser: {method: 'POST', url: BaseUrl + 'users/{id}/{data}'},
  // Programs
  getProgramUrlAccess: { method: 'GET', url: BaseUrl + 'programs/{id}/urlAccess' }, // TODO params not set
  postProgramReportPlayBack: { method: 'POST', url: BaseUrl + 'programs/{id}/reportPlayback{data}' },
  getProgramById: { method: 'GET', url: BaseUrl + 'programs/{id}' },
  postProgramIssue: {method: 'POST', url: BaseUrl + 'programs/{id}/issues{data}'},
  // Archive
  getArchiveUrlAccess: { method: 'GET', url: BaseUrl + 'programs/{id}/urlAccess' }, // TODO params not set
  postArchiveReportPlayBack: { method: 'POST', url: BaseUrl + 'programs/{id}/reportPlayback{data}' },
  getArchiveById: { method: 'GET', url: BaseUrl + 'programs/{id}' },
  // Archive conductor
  getArchiveByQuery: { method: 'GET', url: BaseUrl + 'programs?type=3&query={searchText}' },
  getArchiveList: { method: 'GET', url: BaseUrl + 'programs?count={count}&getFeatured=true&type={type}&offset={offset}&clientPlatform=4' },
  // TV
  getConductorByDate: { method: 'GET', url: BaseUrl + 'channels/{id}/conductor?date={date}' },
  getEpisode: {method: 'GET', url: BaseUrl + 'channels/{channelId}/conductor/{conductorId}/episode'},
  getChannel: {method: 'GET', url: BaseUrl + 'channels/{id}/url'},
  getConductorByChannelId: {method: 'GET', url: BaseUrl + 'channels/{id}/conductor'},
  getChannelCategories: {method: 'GET', url: BaseUrl + 'channelCategories'},
  postTvReportPlayBackByConductorId: {method: 'POST', url: BaseUrl + 'channels/{channelId}/conductor/{conductorId}/reportPlayback{data}'},
  postTvReportPlayBackByChannelId: {method: 'POST', url: BaseUrl + 'channels/{channelId}/reportPlayback{data}'},
  // Conductor
  getPrograms: {method: 'GET', url: BaseUrl + 'programs?count={count}&getFeatured={getFeatured}&type={type}&offset={offset}&clientPlatform=4'},
  // User packages
  getPackages: {method: 'GET', url: BaseUrl + 'users/{id}/packages'},
  getPackageByProgramId: {method: 'GET', url: BaseUrl + 'users/{memberId}/packages?programId={programId}'},
  postPackage: {method: 'POST', url: BaseUrl + 'users/{memberId}/packages{data}'},
  // Notifications
  getNotifications: {method: 'GET', url: BaseUrl + 'users/{id}/notification?count=10&offset=0'},
  postLastNotification: {method: 'POST', url: BaseUrl + 'users/{id}/lastNotification{data}'},
  // Contact us
  postFeedBack: {method: 'POST', url: BaseUrl + 'feedbacks{data}'},
  // TODO fix header
  postContactUs: {method: 'POST', url: BaseUrl + 'contactUs{data}', headers: {'g-recaptcha-response': '{data}'['g-recaptcha-response']}},
  // About us
  // TODO: remove hardcode url
  getAboutUs: {method: 'GET', url: 'http://www.anten.ir/about-us?embeded=1', headers: {Accept: 'text/html'}}
}
