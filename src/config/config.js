export default {
  apiUrl: 'https://api.anten.ir/v3.0/',
  // apiUrl: 'http://94.182.163.118:5321/v3.0/',
  // apiUrl: 'http://nightly.api.anten.ir/v3.0',
  /* eslint-disable spellcheck/spell-checker */
  vastTrackerUrl: 'https://slothmore-ads.farakav.com/vast?clientName=anten',
  gRecaptchaSitekey: '6LerSiAUAAAAAN9BnLSTuuaN9mIUO7T6mvbQlKOt',
  primaryColor: '#eb3a64',
  /* eslint-enable spellcheck/spell-checker */
  locale: 'fa',
  lengthCode: 5,
  resendVerificationCodeTimeout: 60,
  productionTip: false,
  showRelatedPrograms: false,
  NProgress: {
    latencyThreshold: 200, // Number of ms before progressbar starts showing, default: 100,
    router: true, // Show progressbar when navigating routes, default: true
    http: false // Show progressbar when doing Vue.http, default: true
  },
  thirdPartyCredentials: {
    googleAnalytics: 'UA-97890887-11'
  },
  iosAppUrl: 'https://new.sibapp.com/applications/anten?ios',
  androidAppUrl: 'https://cafebazaar.ir/app/com.farakav.anten/?l=fa',
  VASTEnabled: true,
  VASTURL: ['https://vast-ads.farakav.com/anten.xml', 'https://vast-ads.farakav.com/anten2.xml'],
  baseUrl: 'https://anten.ir/'
}
