import Vue from 'vue'
import Moment from 'moment'
// eslint-disable-next-line
import fa from 'moment/locale/fa.js'
import PersianDate from 'persian-date'

Vue.filter('persianDate', function (value, format) {
  if (!value) return ''
  if (!format) {
    format = 'YYYY MM DD'
  }
  let dateValue = new Moment(value).valueOf()
  return new PersianDate(dateValue).format(format)
})

Vue.filter('fromNow', function (value) {
  if (!value) return ''
  return new Moment(value).fromNow()
})

Vue.filter('phone', function (phone) {
  if (!phone) return ''
  return phone.replace(/[^0-9]/g, '')
    .replace(/(\d{4})(\d{3})(\d{4})/, '$1 $2 $3')
})

Vue.filter('price', function (price) {
  if (!price) return ''
  return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
})

Vue.filter('normalizeString', function (text) {
  if (!text) return ''
  const characterMap = {
    '۹': 9,
    '۸': 8,
    '۷': 7,
    '۶': 6,
    '۵': 5,
    '۴': 4,
    '۳': 3,
    '۲': 2,
    '۱': 1,
    '۰': 0,
    'ي': 'ی',
    'ك': 'ک',
    'ة': 'ه'
  }
  text = text.trim()
  const map = Array.prototype.map
  let normalizeText = map.call(text, (char) => {
    if (characterMap[char] !== undefined) {
      char = characterMap[char]
    }
    return char
  })
  return normalizeText.join('')
})

Vue.filter('sanitizeTitle', (title) => {
  if (!title) return ''
  // Trim the last white space
  let slug = title.replace(/\s*$/g, '')
  // Change white space to "-"
  slug = slug.replace(/\s+/g, '-')
  return slug
})
