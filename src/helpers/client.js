import browser from './browser'
class Browser {
  constructor () {
    this.isSamsungBrowser = browser.samsungBrowser
    this.browser = browser
    this.isIOS = browser.ios
    this.isiPad = /iPad/.test(navigator.userAgent) && !window.MSStream
    this.isMSIE = browser.msie
    this.isMobile = browser.mobile
    this.isDesktop = !browser.mobile && !browser.tablet
    this.isTablet = browser.tablet
    this.isAndroid = browser.android
  }
}
export default new Browser()
