import Config from '@/config/config.js'
export default class AdsTracker {
  constructor () {
    this.sequenceToken = ''
  }

  start (data) {
    this.sendRequest({
      'Event': 1,
      'AdsUrl': data.adsUrl,
      'RefId': data.videoId,
      'AdsDuration': -1
    })
  }

  timeout (data) {
    this.sendRequest({
      'Event': 4,
      'AdsUrl': data.adsUrl,
      'RefId': data.videoId,
      'AdsDuration': -1
    })
  }

  finished (data) {
    this.sendRequest({
      'Event': 3,
      'AdsUrl': data.adsUrl,
      'RefId': data.videoId,
      'AdsDuration': -1,
      'SequenceToken': this.sequenceToken
    })
  }

  skipped (data) {
    this.sendRequest({
      'Event': 2,
      'AdsUrl': data.adsUrl,
      'RefId': data.videoId,
      'AdsDuration': -1,
      'SequenceToken': this.sequenceToken
    })
  }

  sendRequest (args) {
    var self = this
    $.ajax({
      url: Config.vastTrackerUrl,
      method: 'POST',
      contentType: 'application/json charset=utf-8',
      dataType: 'json',
      data: JSON.stringify(args),
      success: function (data) {
        self.sequenceToken = data.sequenceToken
      }
    })
  }
}
