/* eslint-disable */
const t = function() {
  const $ = jQuery
  jQuery.fn.closestToOffset = function(offset) {
    let el = null,
      elOffset,
      x = offset.left,
      y = offset.top,
      distance,
      dx,
      dy,
      minDistance
    this.each(function() {
      let $t = $(this)
      let elOffset = $t.offset()
      let right = elOffset.left + $t.width()
      let bottom = elOffset.top + $t.height()

      if (
        x >= elOffset.left &&
        x <= right &&
        y >= elOffset.top &&
        y <= bottom
      ) {
        el = $t
        return false
      }

      let offsets = [
        [elOffset.left, elOffset.top],
        [right, elOffset.top],
        [elOffset.left, bottom],
        [right, bottom]
      ]
      for (let off in offsets) {
        dx = offsets[off][0] - x
        dy = offsets[off][1] - y
        distance = Math.sqrt(dx * dx + dy * dy)
        if (minDistance === undefined || distance < minDistance) {
          minDistance = distance
          el = $t
        }
      }
    })
    return el
  }
  Number.prototype.between = function(a, b) {
    let min = Math.min.apply(Math, [a, b]),
      max = Math.max.apply(Math, [a, b])
    return this > min && this < max
  }
  window.gotTo = function(direction) {
    if (jQuery('.focused').length === 0 && jQuery('[focused]').length === 0) {
      jQuery(jQuery('.focusable')).each(function() {
        if($(this).is(":visible")){
          jQuery(this).addClass('focused').attr('focused')
          return false;
        }
      })
      return false
    }
    const focusableList = []
    jQuery('.focusable').each(function(index, item) {
      const $item = jQuery(item)
      focusableList.push({
        item: $item,
        x: $item.offset().top,
        y: $item.offset().left,
        right: $item.offset().left + $item.outerWidth(),
        bottom: $item.offset().top + $item.outerHeight()
      })
    })
    const tempList = []
    let sibleItem = null
    let $focused = $('.focused, [focused]')
    const currentItem = {
      item: $focused,
      x: $focused.offset().top,
      y: $focused.offset().left,
      right: $focused.offset().left + $focused.outerWidth(),
      bottom: $focused.offset().top + $focused.outerHeight(),
      centerX: $focused.offset().top + ($focused.outerHeight() / 2),
      centerY: $focused.offset().left + ($focused.outerWidth() / 2)
    }
    let filter = function(domain) {
      $.each(focusableList, function(index, item) {
        if (direction === 'top' && item.y.between(currentItem.y - domain, currentItem.y + domain)) {
          if (item.bottom < currentItem.x) {
            tempList.push(item.item)
          }
        }
        if (direction === 'bottom' && item.y.between(currentItem.y - domain, currentItem.y + domain)) {
          if (item.x > currentItem.bottom) {
            tempList.push(item.item)
          }
        }
        if (direction === 'right' && item.x.between(currentItem.x - domain, currentItem.x + domain)) {
          if (item.y > currentItem.right) {
            tempList.push(item.item)
          }
        }
        if (direction === 'left') {
          if (item.right < currentItem.y && item.x.between(currentItem.x - domain, currentItem.x + domain)) {
            tempList.push(item.item)
          }
        }
      })
    }
    filter(30)
    if (tempList.length === 0) {
      filter(200)
    }
    if (tempList.length === 0) {
      filter(200)
    }
    if (tempList.length === 0) {
      filter(500)
    }
    if (tempList.length === 0) {
      filter(800)
    }
    if (tempList.length === 0) {
      filter(1000)
    }
    if (tempList.length === 0) {
      filter(1000000)
    }
    if (direction === 'top') {
      sibleItem = $(tempList).closestToOffset({left: currentItem.centerY, top: currentItem.x})
    }
    if (direction === 'bottom') {
      sibleItem = $(tempList).closestToOffset({left: currentItem.centerY, top: currentItem.bottom})
    }
    if (direction === 'right') {
      sibleItem = $(tempList).closestToOffset({left: currentItem.right, top: currentItem.centerX})
    }
    if (direction === 'left') {
      sibleItem = $(tempList).closestToOffset({left: currentItem.y, top: currentItem.centerX})
    }
    if (sibleItem) {
      $('.focusable.focused').removeClass('focused').removeAttr('focused')
      $('[focused=true').removeAttr('focused')
      sibleItem.addClass('focused').attr('focused', true)
      setTimeout(function() {
        moveCursorToEnd(sibleItem[0]);
      }, 10)
      sibleItem.focus()
    }
  }

  function moveCursorToEnd(el) {
    if (typeof el.selectionStart == "number") {
      el.selectionStart = el.selectionEnd = el.value.length;
    } else if (typeof el.createTextRange != "undefined") {
      el.focus();
      var range = el.createTextRange();
      range.collapse(false);
      range.select();
    }
  }

  document.body.addEventListener('keydown', function(e) {
    switch (event.keyCode) {
      case 39:
        gotTo('right')
        break
      case 37:
        gotTo('left')
        break
      case 38:
        gotTo('top')
        break
      case 40:
        gotTo('bottom')
        break
      case 13:
        if ($('.focused')[0]) {
          $('.focused')[0].click()
        }
        break
      case 8:
        //window.history.back()
        break
        break
    }
  })
}
export default t
