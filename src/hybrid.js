// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import store from '@/store'
import App from '@/ui/hybrid/App.vue'
// import router from '@/ui/hybrid/router'
import VueResource from 'vue-resource'
import NProgress from 'vue-nprogress'
import Notifications from 'vue-notification'
import Config from '@/config/config'
import lodash from 'lodash'
import VueLodash from 'vue-lodash'
import i18n from '@/i18n'
import LorisNavigate from './helpers/loris-navigate'
import Framework7 from 'framework7/dist/framework7.esm.bundle.js'
import Framework7Vue from 'framework7-vue/dist/framework7-vue.esm.bundle.js'

import router from '@hybrid/router'
require('@root/node_modules/framework7/dist/css/framework7.rtl.md.css')
// require('@root/node_modules/framework7/dist/css/framework7.md.css')
Vue.use(Framework7Vue, Framework7)
LorisNavigate()
Vue.use(VueLodash, lodash)
Vue.use(NProgress, Config.NProgress)
Vue.use(Notifications)
Vue.use(VueResource)
Vue.config.productionTip = Config.productionTip
Vue.http.options.root = Config.apiUrl
Vue.http.interceptors.push((request, next) => {
  const token = store.state.auth.token
  request.headers.set('X-CSRF-TOKEN', 'TOKEN')
  request.headers.set('Authorization', `${token}`)
  request.headers.set('Accept', 'application/json')
  next()
})
Vue.http.interceptors.push(function (request, next) {
  next(function (response) {
    if (response.ok === false) {
      store.commit('notify/error', {
        message: response.statusText,
        action: response.body.message
      })
    }
  })
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: {App},
  render: h => h(App),
  store: store,
  framework7: {
    id: 'io.framework7.testApp',
    routes: router
  },
  i18n,
  template: '<App/>'
})
