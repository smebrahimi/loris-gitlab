// https://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint'
  },
  env: {
    browser: true,
  },
  extends: [
    // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
    // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
    'plugin:vue/recommended',
    // https://github.com/standard/standard/blob/master/docs/RULES-en.md
    'standard'
  ],
  globals: {
    jQuery: true,
    $: true
  },
  // required to lint *.vue files
  plugins: [
    'vue',
    'spellcheck'
  ],
  // add your custom rules here
  rules: {
    // allow async-await
    'generator-star-spacing': 'off',
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'spellcheck/spell-checker': [1,
      {

        'comments': true,
        'strings': true,
        'identifiers': true,
        'lang': 'en_US',
        'skipWords': [
          'Anten',
          'farakav',
          'reza',
          'kazupon',
          'slothmore',
          'webkit',
          'urls',
          'Urls',
          'autoplay',
          'vjs',
          'hlsjs',
          'Vee',
          'vee',
          'Validator',
          'eslint',
          'otp',
          'typeof',
          'progressbar',
          'vue',
          'vuex',
          'getters',
          'getter',
          'mixin',
          'mixins',
          'tv',
          'auth',
          'Auth',
          'eslint',
          'persian',
          'js',
          'src',
          'http',
          'https',
          'href',
          'namespaced',
          'Appbar',
          'paramsa',
          'php',
          'Html',
          'svg',
          'img',
          'lottie',
          'msg',
          'params',
          'param',
          'px',
          'php',
          'facebook',
          'ir',
          'Html',
          'html',
          'init',
          'Navbar',
          'navbar',
          'youtube',
          'lang',
          'webpack',
          'lodash',
          'String',
          'api',
          'Recaptcha',
          'todo',
          'viewport',
          'xs',
          'php',
          'www',
          'nav',
          'fb',
          'tw',
          'ios',
          'tg',
          'gp',
          'og',
          'touchmove',
          'resize',
          'ui',
          'runtime',
          'progs',
          'md',
          'os',
          'vuejs',
          'mediapplayer',
          'evt',
          'resized',
          'sitekey',
          'grecaptcha',
          'rtl',
          'li',
          'prev',
          'hardcode',
          'Nprogress',
          'nprogress',
          'bool',
          'pre',
          'Pre',
          'timeshift',
          'playability',
          'conf',
          'hls',
          'Hls',
          'embeded',
          'Analytics',
          'analytics',
          'Debounce',
          'Seo',
          'ok',
          'el',
          'Rflag',
          'mediaplayer',
          'png',
          'jpg',
          'xml',
          'bmp',
          'ltr',
          'br',
          'Signup',
          'broadcasted',
          'io',
          'esm',
          'github',
          'Github',
          'samsung',
          'utf',
          'ajax',
          'args',
          'sibapp',
          'cafebazaar',
          'videojs',
          'charset',
          'isi',
          'msie',
          'dddd',
          'rightel',
          'sib',
          'donut',
          'Dropdown',
          'feedbacks', // return from server
          'udid', // post to server
          'Scrollbar', // for library
          'ded', // used of browser library
          'Diaz', // used of browser library
          'Bowser', // used of browser library
          'bowser', // used of browser library
          'keyboardshow', // used of addEventListener
          'uniq', // lodash
          'Timeline',
          'toman'
        ]
      }
    ]
  }
}
