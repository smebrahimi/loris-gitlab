module.exports = {
  logout: function (browser) {
    let isMobile = this.isMobileBrowser(browser)
    browser.perform(function () {
      if (isMobile.includes(true)) {
        console.log('-------------isMobileBrowser-----------------')
        browser
          .waitForElementVisible('[e2e-nav-button]', 10000, false)
          .click('[e2e-nav-button]')
          .waitForElementVisible('[e2e-logout-s]', 10000, false)
          .click('[e2e-logout-s]')
          .pause(3000)
          .waitForElementVisible('[e2e-logout-modal]', 10000, false)
          .pause(2000)
          .waitForElementVisible('[e2e-logout-exit]', 10000, false)
          .click('[e2e-logout-exit]')
          .pause(3000)
        return this
      } else if (isMobile.includes(false))  {
        console.log('-------------isDesktopBrowser-----------------')
        browser
          .waitForElementVisible('[e2e-profile-menu]', 10000, false)
          .click('[e2e-profile-menu]')
          .waitForElementVisible('[e2e-logout]', 10000, false)
          .click('[e2e-logout]')
          .pause(3000)
          .waitForElementVisible('[e2e-logout-modal]', 10000, false)
          .pause(2000)
          .waitForElementVisible('[e2e-logout-exit]', 10000, false)
          .click('[e2e-logout-exit]')
          .pause(3000)
        return this
      }
    })
  },
  shot: function (browser, name, arg) {
    if (browser.capture) {
      return browser.capture(name)
    } else {
      return browser.saveScreenshot('./test/e2e/main-test/local_reports/screenshots/' + arg + '/' + name)
    }
  },
  getBaseUrl: function (browser) {
    let devServer = browser.globals.devServerURL
    if (!devServer) {
      devServer = browser.globals.test_settings.devServerURL
    }
    return devServer
  },
  isGerbilBrowser: function (browser) {
    if (browser.globals.test_settings.devServerURL) {
      return true
    } else {
      return false
    }
  },
  isLocalBrowser: function (browser) {
    if (!browser.globals.test_settings.devServerURL) {
      return true
    } else {
      return false
    }
  },
  isMobileBrowser: function (browser) {
    let output = []
    browser.getElementSize('body', function (result) {
      console.log('----------------Width is :' + result.value.width)
      if (result.value.width <= 767) {
        output.push(true)
      } else if (result.value.width > 767) {
        output.push(false)
      }
    })
    return output
  },
  isTabletBrowser: function (browser) {
    let output = []
    browser.getElementSize('body', function (result) {
      console.log('----------------Width is :' + result.value.width)
      if (result.value.width <= 992) {
        output.push(true)
      } else if (result.value.width > 992) {
        output.push(false)
      }
    })
    return output
  },
  login: function (browser) {
    let devServer = this.getBaseUrl(browser)
    let isMobile = this.isMobileBrowser(browser)
    browser
      .url(devServer)
    browser.perform(function () {
      if (isMobile.includes(true)) {
        console.log('-------------isMobileBrowser-----------------')
        browser
          .waitForElementVisible('[e2e-login-mobile]', 10000, false)
          .click('[e2e-login-mobile]')
          .waitForElementVisible('[e2e-phone-number-input-modal]', 10000)
          .click('[e2e-phone-number-input-modal]')
          .setValue('[e2e-phone-number-input-modal]', '09377770077')
          .waitForElementVisible('[e2e-send-phone-number-button-modal]', 10000)
          .click('[e2e-send-phone-number-button-modal]')
          .pause(5000)
          .waitForElementVisible('[e2e-verify-code-input-modal]', 50000)
          .setValue('[e2e-verify-code-input-modal]', '85900')
          .waitForElementVisible('[e2e-submit-verify-code-button-modal]', 10000)
          .click('[e2e-submit-verify-code-button-modal]')
          .pause(5000)
        return this
      } else if (isMobile.includes(false))  {
        console.log('-------------isDesktopBrowser-----------------')
        browser
          .waitForElementVisible('[e2e-login-desktop]', 10000, false)
          .click('[e2e-login-desktop]')
          .waitForElementVisible('[e2e-phone-number-input-modal]', 10000)
          .click('[e2e-phone-number-input-modal]')
          .setValue('[e2e-phone-number-input-modal]', '09377770077')
          .waitForElementVisible('[e2e-send-phone-number-button-modal]', 10000)
          .click('[e2e-send-phone-number-button-modal]')
          .pause(5000)
          .waitForElementVisible('[e2e-verify-code-input-modal]', 50000)
          .setValue('[e2e-verify-code-input-modal]', '85900')
          .waitForElementVisible('[e2e-submit-verify-code-button-modal]', 10000)
          .click('[e2e-submit-verify-code-button-modal]')
          .pause(5000)
        return this
      }
    })
    return this
  }

}
