let volumeBarDimension = null
module.exports = {
  elements: {
    Video: {
      selector: '#Program-12966',
      locateStrategy: 'css selector'
    },
    PlayerPlayerPlayButton: {
      selector: 'button.vjs-play-control.vjs-control.vjs-button.vjs-paused',
      locateStrategy: 'css selector'
    },
    PlayerPlayerPauseButton: {
      selector: 'button.vjs-play-control.vjs-control.vjs-button.vjs-playing',
      locateStrategy: 'css selector'
    },
    PlayerPlayerPlayBox: {
      selector: '.video-js',
      locateStrategy: 'css selector'
    },
    PlayePlayerQulityLevel: {
      selector: '//span[@class="vjs-resolution-button-label"]',
      localStrategy: 'xpath'
    },
    PlayerPlayerQualityLevelAuto: {
      selector: '//span[@class="vjs-menu-item-text"][contains(text(),"اتوماتیک")]',
      localStrategy: 'xpath'
    },
    PlayerPlayerQualityLevel270: {
      selector: '//span[contains(text(),"270p")]',
      localStrategy: 'xpath'
    },
    PlayerPlayerQualityLevel480: {
      selector: '//span[contains(text(),"480p")]',
      localStrategy: 'xpath'
    },
    PlayerPlayerQualityLevel540: {
      selector: '//span[contains(text(),"540p")]',
      localStrategy: 'xpath'
    },
    PlayerPlayerQualityLevel720: {
      selector: '//span[contains(text(),"720p")]',
      localStrategy: 'xpath'
    },
    // PlayerPlayerQualityLevel540: {
    //   selector: 'div.vjs-control-bar > div.vjs-menu-button.vjs-menu-button-popup.vjs-control.vjs-button.vjs-resolution-button > div > ul > li:nth-child(0)'
    // },
    // PlayerPlayerQualityLevel720: {
    //   selector: 'div.vjs-control-bar > div.vjs-menu-button.vjs-menu-button-popup.vjs-control.vjs-button.vjs-resolution-button > div > ul > li:nth-child(0)'
    // },
    PlayerPlayerFullScreenButton: {
      selector: 'div.vjs-control-bar > button.vjs-fullscreen-mode-btn.vjs-icon-fullscreen-enter',
      locateStrategy: 'css selector'
    },
    PlayerPlayerExitFullScreenButton: {
      selector: 'div.vjs-control-bar > button.vjs-fullscreen-mode-btn.vjs-icon-fullscreen-exit',
      locateStrategy: 'css selector'
    },
    PlayerPlayerMuteButton: {
      selector: 'div.vjs-control-bar > div.vjs-volume-panel.vjs-control.vjs-volume-panel-horizontal > button',
      locateStrategy: 'css selector'
    },
    PlayerPlayerUnmuteButton: {
      selector: 'div.vjs-control-bar > div.vjs-volume-panel.vjs-control.vjs-volume-panel-horizontal > button',
      locateStrategy: 'css selector'
    },
    PlayerPlayerVolumeLevel: {
      selector: '.vjs-volume-level',
      locateStrategy: 'css selector'
    },
    PlayerPlayerVolumeBar: {
      selector: '.vjs-volume-menu-button .vjs-menu-content',
      locateStrategy: 'css selector'
    },
    volumeButton: {
      selector: 'div.vjs-control-bar > div.vjs-volume-menu-button.vjs-menu-button.vjs-menu-button-popup.vjs-control.vjs-button',
      locateStrategy: 'css selector'
    },
    NextTimeVideo: {
      selector: '#program-11737',
      locateStrategy: 'css selector'
    },
    PlayerPlayerPlayBoxNextTime: {
      selector: '.template-pre-container .box.margin-bottom-half.relative.clr.is-transparent .preview.patterned-bg',
      locateStrategy: 'css selector'
    }
  },
  commands: [{
    hoverOnVideoPlayerContainer: function (browser, callback) {
      browser
        .execute(function () {
          window.document.querySelector('.video-js').classList.remove('vjs-user-inactive')
          window.document.querySelector('.video-js').classList.add('vjs-user-active')
        }, [], callback)

      return this
    },
    getVolumeBarDimensions: function (callback) {
      const player = this.api.page.player()
      if (volumeBarDimension) {
        callback(volumeBarDimension)
        return this
      }
      player.getElementSize('@PlayerPlayerVolumeBar', (result) => {
        // this.assert.ok(typeof result === 'object', 'I got the volume bar element')
        this.assert.ok(result.value.height > 0, 'Volume bar height is positive')
        volumeBarDimension = {width, height} = result.value
        callback(volumeBarDimension)
      })
      return this
    },
    seekOnVolumeBar: function (volumePercent, callback) {
      const player = this.api.page.player()
      const _this = this
      player.assert.visible('@PlayerPlayerVolumeBar')
      player.getVolumeBarDimensions(function (dimension) {
        console.log('------------------------')
        console.log(volumePercent)
        const {width, height} = dimension
        player.PAUSE(3000)
        player.moveToElement('@PlayerPlayerVolumeBar', width / 2, height - (volumePercent * height), function () {
          _this.api
            .mouseButtonClick(0, callback)
        })
      })
      return this
    },
    PAUSE: function (duration) {
      this.api.pause(duration || this.props.pauseDuration)
      return this
    }
  }],
  props: {
    pauseDuration: 10000
  }
}
