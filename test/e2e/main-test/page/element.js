let volumeBarDimension = null
module.exports = {
  elements: {
    SelectFirstProgram: {
      selector: '#app > div.archive-page > div:nth-child(2) > div > div > div > div > div:nth-child(2) > div:nth-child(2)'
    },
    FirstSearchResult: {
      selector: 'div.programs-list.col-3 > section > div:nth-child(3) > div'
    },
    Tv1:{
      // selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(1)'
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(1)'
    },
    Tv2:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(2)'
    },
    Tv3:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(3)'
    },
    Tv4:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(4)'
    },
    TvTehran:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(5)'
    },
    TvKhabar:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(6)'
    },
    TvVarzesh:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(7)'
    },
    TvIfilm:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(9)'
    },
    TvNasim:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(8)'
    },
    TvNemayesh:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(10)'
    },
    TvTamasha:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(11)'
    },
    TvOfogh:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(12)'
    },
    TvQoran:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(13)'
    },
    TvPooya:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(14)'
    },
    TvOmid:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(15)'
    },
    TvIranKala:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(16)'
    },
    TvMostanad:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(17)'
    },
    TvShoma:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(18)'
    },
    TvAmoozesh:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(19)'
    },
    TvSalamat:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(20)'
    },
    TvTv:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(21)'
    },
    TvAlalam:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(22)'
    },
    TvKosar:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(23)'
    },
    TvJamjam:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(24)'
    },
    TvSepehr:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(25)'
    },
    TvKhorasan:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(27)'
    },
    TvKhozestan:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(28)'
    },
    TvMazandaran:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(29)'
    },
    TvSahand:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(30)'
    },
    TvBaran:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(31)'
    },
    TvEsfahan:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(32)'
    },
    TvFars:{
      selector: 'div.component-live-tv-conductor > div > div > div > div:nth-child(33)'
    }
  }
}
