module.exports = {
  before: function (browser) {
    let helper, isLocalBrowser
    helper = browser.page.helper()
    browser.perform(function () {
      isLocalBrowser = helper.isLocalBrowser(browser)
      if (isLocalBrowser) {
        browser.resizeWindow(375, 767)
      }
    })
  },
  'step 1 -notification Page': function (browser) {
    let helper, isGerbilBrowser, isMobileBrowser, isLocalBrowser, devServer
    helper = browser.page.helper()
    devServer = helper.getBaseUrl(browser)
    browser.url(devServer, function(result) {       if ( result.status == -1) {         console.log('page not found');         browser.end()       }       console.log(devServer);       console.log(result);     });
    browser.perform(function () {
      isGerbilBrowser = helper.isGerbilBrowser(browser)
      isMobileBrowser = helper.isMobileBrowser(browser)
      isLocalBrowser = helper.isLocalBrowser(browser)
    })
    browser.waitForElementVisible('#app', 10000)
    helper.login(browser)
    browser.pause(10000)
    helper.shot(browser, '01-init-home-page.png', '04-notification')
    browser.perform(function () {
      console.log('isMobileBrowser : ' + isMobileBrowser)
      console.log('isGerbilBrowser : ' + isGerbilBrowser)
      console.log('isLocalBrowser : ' + isLocalBrowser)
      if (((isGerbilBrowser && isMobileBrowser) === true) || (isLocalBrowser && isMobileBrowser)) {
        console.log('-------------isMobileBrowser-----------------')
        browser.waitForElementVisible('[e2e-nav-button]', 3000)
        browser.click('[e2e-nav-button]')
        helper.shot(browser, 'nav-button.png', '04-notification')
        browser.waitForElementVisible('[e2e-notification-s]', 3000)
        browser.click('[e2e-notification-s]')
        browser.pause(3000)
        helper.shot(browser, '02-notification.png', '04-notification')
      } else {
        browser.waitForElementVisible('[e2e-profile-menu]', 5000, false)
        browser.click('[e2e-profile-menu]')
        helper.shot(browser, 'menu.png', '04-notification')
        browser.waitForElementVisible('[e2e-notification]', 3000)
        browser.click('[e2e-notification]')
        browser.pause(3000)
        helper.shot(browser, '02-notification.png', '04-notification')
      }
    })
    helper.logout(browser)
    browser.end()
  }
}
