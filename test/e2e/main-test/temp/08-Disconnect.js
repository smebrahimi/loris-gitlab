module.exports = {
  before: function (browser) {
    let helper, isLocalBrowser
    helper = browser.page.helper()
    browser.perform(function () {
      isLocalBrowser = helper.isLocalBrowser(browser)
      if (isLocalBrowser) {
        browser.resizeWindow(375, 767)
      }
    })
  },
  'step 1 -connect Page': function (browser) {
    let helper, isGerbilBrowser, isMobileBrowser, isLocalBrowser, devServer
    helper = browser.page.helper()
    devServer = helper.getBaseUrl(browser)
    browser.url(devServer, function(result) {       if ( result.status == -1) {         console.log('page not found');         browser.end()       }       console.log(devServer);       console.log(result);     });
    // browser.waitForElementVisible('[e2e-countdown]', 15000, false, function (res) {
    //   if (res.value) {
    //     console.log('No program playing try later ............')
    //     console.log(res)
    //     browser.end()
    //   } else {
    //     console.log('program is playing............')
    //   }
    // })
    browser.perform(function () {
      isGerbilBrowser = helper.isGerbilBrowser(browser)
      isMobileBrowser = helper.isMobileBrowser(browser)
      isLocalBrowser = helper.isLocalBrowser(browser)
    })
    browser.waitForElementVisible('#app', 10000)
    helper.login(browser)
    browser.url(devServer + '/archive')
    browser.pause(10000)
    helper.shot(browser, '01-init-home-page.png', '08-disconnect')
    browser.perform(function () {
      console.log('isMobileBrowser : ' + isMobileBrowser)
      console.log('isGerbilBrowser : ' + isGerbilBrowser)
      console.log('isLocalBrowser : ' + isLocalBrowser)
    })
    browser.end()
  },
  'step 2 -Disconnect2 Page': function (browser) {
    let helper, isGerbilBrowser, isMobileBrowser, isLocalBrowser, devServer
    browser.perform(function () {
      isLocalBrowser = helper.isLocalBrowser(browser)
      if (isLocalBrowser) {
        browser.resizeWindow(375, 767)
      }
    })
    helper = browser.page.helper()
    devServer = helper.getBaseUrl(browser)
    browser.url(devServer, function(result) {       if ( result.status == -1) {         console.log('page not found');         browser.end()       }       console.log(devServer);       console.log(result);     });
    // browser.waitForElementVisible('[e2e-countdown]', 15000, false, function (res) {
    //   if (res.value) {
    //     helper.logout(browser)
    //     browser.end()
    //   }
    // })
    browser.perform(function () {
      isGerbilBrowser = helper.isGerbilBrowser(browser)
      isMobileBrowser = helper.isMobileBrowser(browser)
      isLocalBrowser = helper.isLocalBrowser(browser)
    })
    browser.waitForElementVisible('#app', 10000)
    helper.login(browser)
    browser.url(devServer + '/archive')
    browser.pause(30000)
    helper.shot(browser, '02-init-home-page.png', '08-disconnect')
    browser.perform(function () {
      console.log('isMobileBrowser : ' + isMobileBrowser)
      console.log('isGerbilBrowser : ' + isGerbilBrowser)
      console.log('isLocalBrowser : ' + isLocalBrowser)
    })
    browser.waitForElementVisible('[e2e-killOtherSessions]', 50000)
    browser.click('[e2e-killOtherSessions]')
    browser.pause(2000)
    helper.shot(browser, '03-killOtherSessions.png', '08-disconnect')
    helper.logout(browser)
    browser.end()
  }
}
