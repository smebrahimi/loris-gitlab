module.exports = {
  before (browser) {
    let helper, isLocalBrowser
    helper = browser.page.helper()
    browser.perform(() => {
      isLocalBrowser = helper.isLocalBrowser(browser)
      if (isLocalBrowser) {
        browser.resizeWindow(375, 767)
      }
    })
  },
  'step 1 -live page' (browser) {
    let helper, isGerbilBrowser, isMobileBrowser, isLocalBrowser, devServer
    helper = browser.page.helper()
    devServer = helper.getBaseUrl(browser)
    browser.url(devServer, function(result) {       if ( result.status == -1) {         console.log('page not found');         browser.end()       }       console.log(devServer);       console.log(result);     });
    browser.perform(() => {
      isGerbilBrowser = helper.isGerbilBrowser(browser)
      isLocalBrowser = helper.isLocalBrowser(browser)
      isMobileBrowser = helper.isMobileBrowser(browser)
    })
    browser.waitForElementVisible('#app', 10000)
    helper.shot(browser, '01-init-home-page.png', '10-live')
    browser.perform(() => {
      console.log(`isMobileBrowser : ${isMobileBrowser}
                   isGerbilBrowser : ${isGerbilBrowser}
                   isLocalBrowser  : ${isLocalBrowser}`)
      if (((isGerbilBrowser && isMobileBrowser) === false) || (isLocalBrowser && isMobileBrowser)) {
        console.log('-------------isMobileBrowser-----------------')
        browser.waitForElementVisible('[e2e-live]', 5000)
        browser.click('[e2e-live]')
        browser.pause(3000)
        helper.shot(browser, '02-e2e-live.png', '10-live')
      } else {
        console.log('-------------isnotMobileBrowser-----------------')
        browser.waitForElementVisible('[e2e-live-h]', 5000)
        browser.click('[e2e-live-h]')
        browser.pause(3000)
        helper.shot(browser, '02-e2e-live.png', '10-live')
      }
    })
  },
  'step 2 - Player' (browser) {
    let helper = browser.page.helper()
    let player = browser.page.player()
    browser.waitForElementVisible('a[href="/tv/3"]', 10000, false)
    browser.click('a[href="/tv/3"]')
    browser.pause(5000)
    // -------------------------------paly & pause with paly & pause Button---------------------------
    player.waitForElementVisible('@PlayerPlayerPlayBox', 10000, false)
    player.assert.elementPresent('@PlayerPlayerPlayBox', 10000)
    browser.pause(5000)
    helper.shot(browser, '03-Init-paly-and-pause.png', '10-live')
    player.hoverOnVideoPlayerContainer(browser)
    player.waitForElementVisible('@PlayerPlayerPauseButton', 10000, false)
    player.click('@PlayerPlayerPauseButton')
    browser.pause(2000)
    helper.shot(browser, '04-pauseVideoButton.png', '10-live')
    // -------------------------------Paly & Pause With Paly & Pause click------------------------------
    player.hoverOnVideoPlayerContainer(browser)
    player.click('@PlayerPlayerPlayBox')
    helper.shot(browser, '05-pauseVideoClick.png', '10-live')
    player.hoverOnVideoPlayerContainer(browser)
    player.click('@PlayerPlayerPlayBox')
    browser.pause(2000)
    helper.shot(browser, '06-playVideoClick.png', '10-live')
    // -------------------------------------Video Quality Level------------------------------------------
    player.hoverOnVideoPlayerContainer(browser)
    player.click('@PlayerPlayerPlayBox')
    player.waitForElementVisible('@PlayePlayerQulityLevel', 15000, false)
    player.click('@PlayePlayerQulityLevel')
    browser.pause(2000)
    helper.shot(browser, '07-hoverOnVideoPlayerResolutionMenu.png', '10-live')
    player.hoverOnVideoPlayerContainer(browser)
    player.click('@PlayerPlayerPlayBox')
    player.click('@PlayePlayerQulityLevel')
    player.click('@PlayerPlayerQualityLevelAuto')
    browser.pause(2000)
    helper.shot(browser, '08-hoverOnVideoPlayerResolutionSelected.png', '10-live')
    player.hoverOnVideoPlayerContainer(browser)
    player.click('@PlayerPlayerPlayBox')
    player.click('@PlayePlayerQulityLevel')
    player.click('@PlayerPlayerQualityLevel270')
    browser.pause(2000)
    helper.shot(browser, '09-hoverOnVideoPlayerResolution270.png', '10-live')
    player.hoverOnVideoPlayerContainer(browser)
    player.click('@PlayerPlayerPlayBox')
    player.click('@PlayePlayerQulityLevel')
    player.click('@PlayerPlayerQualityLevel480')
    browser.pause(2000)
    helper.shot(browser, '10-hoverOnVideoPlayerResolution480.png', '10-live')
    player.hoverOnVideoPlayerContainer(browser)
    player.click('@PlayerPlayerPlayBox')
    player.click('@PlayePlayerQulityLevel')
    player.click('@PlayerPlayerQualityLevel540')
    browser.pause(2000)
    helper.shot(browser, '11-hoverOnVideoPlayerResolution540.png', '10-live')
    player.hoverOnVideoPlayerContainer(browser)
    player.click('@PlayerPlayerPlayBox')
    player.click('@PlayePlayerQulityLevel')
    player.click('@PlayerPlayerQualityLevel720')
    browser.pause(2000)
    helper.shot(browser, '12-hoverOnVideoPlayerResolution720.png', '10-live')
    // ------------------------------------Screen size------------------------------------
    player.hoverOnVideoPlayerContainer(browser)
    player.click('@PlayerPlayerFullScreenButton')
    helper.shot(browser, '13-FullScreenButton.png', '10-live')
    player.hoverOnVideoPlayerContainer(browser)
    player.click('@PlayerPlayerFullScreenButton')
    browser.pause(2000)
    helper.shot(browser, '14-ExitFullScreenButton.png', '10-live')
    // -----------------------------------Volume-----------------------------------------
    player.hoverOnVideoPlayerContainer(browser)
    player.click('@PlayerPlayerMuteButton')
    browser.pause(2000)
    helper.shot(browser, '15-PlayerMuteButton.png', '10-live')
    player.hoverOnVideoPlayerContainer(browser)
    player.click('@PlayerPlayerUnmuteButton')
    browser.pause(2000)
    helper.shot(browser, '16-PlayerUnmuteButton.png', '10-live')
    // -----------------------------------Volume Level-------------------------------------*/
    player.assert.elementPresent('@PlayerPlayerPlayBox', 10000)
    player.assert.elementPresent('@PlayerPlayerVolumeLevel', 10000)
    browser.pause(3000)
    browser.execute(() => {
      let $video = jQuery('.video-js')
      setInterval(function () {
        $video.removeClass('vjs-user-inactive')
        $video.addClass('vjs-user-active')
      }, 2000)
      jQuery('.vjs-volume-menu-button .vjs-menu-content').show().css({
        width: '2.9em'
      })
      helper.shot(browser, '17-Init-Volume-Level.png', '10-live')
    }, [], () => {
      player.PAUSE(15000)
      player.assert.visible('@PlayerPlayerVolumeLevel')
      player.seekOnVolumeBar(0.5, () => {
        browser.pause(1000)
        helper.shot(browser, '18-Set-Volume-0.2.png', '10-live')
        player.PAUSE()
      })
      browser.end()
    })
  }
}
