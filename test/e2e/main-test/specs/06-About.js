module.exports = {
  before: function (browser) {
    let helper, isLocalBrowser
    helper = browser.page.helper()
    browser.perform(function () {
      isLocalBrowser = helper.isLocalBrowser(browser)
      if (isLocalBrowser) {
        browser.resizeWindow(375, 767)
      }
    })
  },
  'step 1 - About us page': function (browser) {
    let helper, isMobileBrowser, devServer
    helper = browser.page.helper()
    devServer = helper.getBaseUrl(browser)
    browser.url(devServer, function(result) {
      if ( result.status == -1) {
        console.log( devServer + 'page not found');
        browser.end()
      }
    });
    browser.perform(function () {
      isMobileBrowser = helper.isMobileBrowser(browser)
    })
    browser.waitForElementVisible('body', 10000)
    helper.login(browser)
    browser.pause(10000)
    helper.shot(browser, '01-init-home-page.png', '06-about-us')
    browser.perform(function () {
      console.log('isMobileBrowser : ' + isMobileBrowser)
      if (isMobileBrowser.includes(true)) {
        console.log('-------------isMobileBrowser-----------------')
        browser.waitForElementVisible('[e2e-nav-button]', 3000)
        browser.click('[e2e-nav-button]')
        helper.shot(browser, 'nav-button.png', '06-about-us')
        browser.waitForElementVisible('[e2e-about-us-s]', 3000)
        browser.click('[e2e-about-us-s]')
        browser.pause(3000)
        helper.shot(browser, '02-about-us.png', '06-about-us')
      } else if (isMobileBrowser.includes(false)) {
        browser.waitForElementVisible('[e2e-aboutus-footer]', 3000)
        browser.click('[e2e-aboutus-footer]')
        browser.pause(3000)
        helper.shot(browser, '02-about-us.png', '06-about-us')
      }
    })
    helper.logout(browser)
    browser.end()
  }
}
