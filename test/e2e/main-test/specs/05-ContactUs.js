module.exports = {
  before: function (browser) {
    let helper, isLocalBrowser
    helper = browser.page.helper()
    browser.perform(function () {
      isLocalBrowser = helper.isLocalBrowser(browser)
      if (isLocalBrowser) {
        browser.resizeWindow(375, 767)
      }
    })
  },
  'step 1 - contact-us page': function (browser) {
    let helper, isMobileBrowser, devServer
    helper = browser.page.helper()
    devServer = helper.getBaseUrl(browser)
    browser.url(devServer, function(result) {
      if ( result.status == -1) {
        console.log( devServer + 'page not found');
        browser.end()
      }
    });
    browser.perform(function () {
      isMobileBrowser = helper.isMobileBrowser(browser)
    })
    browser.waitForElementVisible('body', 10000)
    helper.login(browser)
    browser.pause(10000)
    helper.shot(browser, '01-init-home-page.png', '05-contact-us')
    browser.perform(function () {
      console.log('isMobileBrowser : ' + isMobileBrowser)
      if (isMobileBrowser.includes(true)) {
        console.log('-------------isMobileBrowser-----------------')
        browser.waitForElementVisible('[e2e-nav-button]', 6000)
        browser.click('[e2e-nav-button]')
        helper.shot(browser, 'nav-button.png', '05-contact-us')
        browser.waitForElementVisible('[e2e-contact-us-s]', 6000)
        browser.click('[e2e-contact-us-s]')
        browser.pause(3000)
        helper.shot(browser, '02-contact-us.png', '05-contact-us')
      } else  if (isMobileBrowser.includes(false)) {
        browser.waitForElementVisible('[e2e-contactus-footer]', 5000)
        browser.click('[e2e-contactus-footer]')
        browser.pause(3000)
        helper.shot(browser, '02-contact-us.png', '05-contact-us')
      }
    })
    browser.waitForElementVisible('[e2e-msg-text]', 6000)
    browser.setValue('[e2e-msg-text]', 'تست صفحه ارتباط با ما در حالت لاگین ( لطفا در صورت بررسی این پیام را نادیده بگیرید )')
    browser.pause(2000)
    helper.shot(browser, '03-msg-text.png', '05-contact-us')
    browser.waitForElementVisible('[e2e-msg-send-a]', 6000)
    browser.click('[e2e-msg-send-a]')
    browser.waitForElementVisible('.notifications', 6000, true, function () {
      helper.shot(browser, '04-send-msg-notif.png', '05-contact-us')
    })
    browser.pause(5000)
    browser.waitForElementVisible('[e2e-msg-send-a]', 6000)
    browser.click('[e2e-msg-send-a]')
    helper.shot(browser, '05-empty-send-msg.png', '05-contact-us')
    helper.logout(browser)
    // -------------------------------without login-----------------------------------------
    browser.perform(function () {
      console.log('isMobileBrowser : ' + isMobileBrowser)
      if (isMobileBrowser.includes(true)) {
        console.log('-------------isMobileBrowser-----------------')
        browser.waitForElementVisible('[e2e-nav-button]', 6000)
        browser.click('[e2e-nav-button]')
        helper.shot(browser, 'nav-button.png', '05-contact-us')
        browser.waitForElementVisible('[e2e-contact-us-s]', 6000)
        browser.click('[e2e-contact-us-s]')
        browser.pause(3000)
      } else  if (isMobileBrowser.includes(false)) {
        browser.waitForElementVisible('[e2e-contactus-footer]', 6000)
        browser.click('[e2e-contactus-footer]')
        browser.pause(3000)
      }
    })
    helper.shot(browser, '06-empty-msg-without login.png', '05-contact-us')
    browser.waitForElementVisible('[e2e-msg-send-a]', 6000)
    browser.click('[e2e-msg-send-a]')
    helper.shot(browser, '07-click-empty-msg-without login.png', '05-contact-us')
    browser.setValue('[e2e-msg-name]', 'تست 1234')
    browser.setValue('[e2e-msg-mobile]', 'تست ')
    browser.setValue('[e2e-msg-email]', 'تست email')
    browser.setValue('[e2e-msg-text]', 'تست صفحه ارتباط با ما در حالت غیر لاگین ( لطفا در صورت بررسی این پیام را نادیده بگیرید )')
    browser.waitForElementVisible('[e2e-msg-send-a]', 6000)
    browser.click('[e2e-msg-send-a]')
    helper.shot(browser, '08-click-msg-without login.png', '05-contact-us')
    browser.end()
  }
}
