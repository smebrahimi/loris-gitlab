module.exports = {
  before: function (browser) {
    let helper, isLocalBrowser
    helper = browser.page.helper()
    browser.perform(function () {
      isLocalBrowser = helper.isLocalBrowser(browser)
      if (isLocalBrowser) {
        browser.resizeWindow(375, 767)
      }
    })
  },
  //----------------------------login with modal home page---------------------------------
  'step 01 -Enter & Check Phone Number fast login': function (browser) {
    let helper = browser.page.helper()
    let devServer = helper.getBaseUrl(browser)
    let isMobile = helper.isMobileBrowser(browser)

    browser.url(devServer, function(result) {
      if ( result.status == -1) {
        console.log( devServer + 'page not found');
        browser.end()
      }
    });
    browser.waitForElementVisible('body', 10000, false, function () {}, 'body Present ')
    helper.shot(browser, '01-init-home-page.png', '01-Loginpage')
    // ----------------------Test Invalid Phone Number------------------------
    browser.perform(function () {
      if (isMobile.includes(true)) {
        console.log('-------------isMobileBrowser-----------------')
        browser
          .waitForElementVisible('[e2e-login-mobile]', 10000)
          .click('[e2e-login-mobile]')
      } else if (isMobile.includes(false))  {
        console.log('-------------isDesktopBrowser-----------------')
        browser
          .waitForElementVisible('[e2e-login-desktop]', 10000)
          .click('[e2e-login-desktop]')
      }
    })
    browser.waitForElementVisible('[e2e-login-modal]', 10000)
    browser.pause(2000)
    helper.shot(browser, '02-login-modal.png', '01-Loginpage')
    browser.waitForElementVisible('[e2e-phone-number-input-modal]', 10000, false, function () {}, '#phone number input Present')
    browser.click('[e2e-phone-number-input-modal]')
    browser.setValue('[e2e-phone-number-input-modal]', '۵۵۶۶۴۴')
    helper.shot(browser, '03-invalid-phone-number-modal.png', '01-Loginpage')
    browser.waitForElementVisible('[e2e-send-phone-number-button-modal]', 10000)
    browser.click('[e2e-send-phone-number-button-modal]')
    browser.pause(2000)
    browser.waitForElementVisible('.notifications', 10000, false, function () {
      helper.shot(this, '04-invalid-phone-number-notif-modal.png', '01-Loginpage')
    })
    browser.pause(3000)
    // ----------------------Test Valid Phone Number------------------------
    browser.clearValue('[e2e-phone-number-input-modal]')
    browser.setValue('[e2e-phone-number-input-modal]', '09377770077')
    browser.pause(3000)
    helper.shot(browser, '05-valid-phone-number-modal.png', '01-Loginpage')
    browser.waitForElementVisible('[e2e-send-phone-number-button-modal]', 10000)
    browser.click('[e2e-send-phone-number-button-modal]', function () {
      console.log('clicked on e2e-send-phone-number-button , NumberInput value is 09377770077')
    })
    browser.pause(2000)
    helper.shot(browser, '06-init-verify-code-step-modal.png', '01-Loginpage')
  },
  'step 02 -Enter & Check Verify Code fast login': function (browser) {
    let helper = browser.page.helper()
    browser.pause(5000)
    // ------------------------------ Test Resend Verify Code ------------------------
    browser.waitForElementVisible('[e2e-resend-verify-code-button-modal]', 71000)
    browser.getText('[e2e-resend-verify-code-button-modal]', function (result) {
      console.log(result.value)
    })
    helper.shot(browser, '07-resend-button-after60s-modal.png', '01-Loginpage')
    browser.pause(2000)
    browser.click('[e2e-resend-verify-code-button-modal]')
    browser.waitForElementVisible('.notifications', 10000, false, function () {
      browser.pause(1000)
      helper.shot(this, '08-resend-code-notif-modal.png', '01-Loginpage')
    }, 'Code Resend after 60s Notification Present')
    browser.pause(6000)
    // ------------------------------ Test Invalid Verify Code -----------------------
    browser.waitForElementVisible('[e2e-verify-code-input-modal]', 10000)
    browser.setValue('[e2e-verify-code-input-modal]', '85')
    helper.shot(browser, '09-invalid-verify-code-modal.png', '01-Loginpage')
    browser.pause(3000)
    browser.waitForElementVisible('[e2e-submit-verify-code-button-modal]', 10000)
    browser.click('[e2e-submit-verify-code-button-modal]', function () {
      console.log('clicked on e2e-verify-code-submit-button , Verify Code value is 85')
    })
    browser.waitForElementVisible('.notifications', 20000, false, function () {
      helper.shot(this, '10-invalid-verify-code-modal-notif.png', '01-Loginpage')
    })
    // ------------------------------ Test Valid Verify Code -----------------------
    browser.pause(3000)
    browser.waitForElementVisible('[e2e-verify-code-input-modal]', 10000)
    browser.setValue('[e2e-verify-code-input-modal]', '900')
    helper.shot(browser, '11-valid-verify-code-modal.png', '01-Loginpage')
    browser.waitForElementVisible('[e2e-submit-verify-code-button-modal]', 10000)
    browser.click('[e2e-submit-verify-code-button-modal]', function () {
      console.log('clicked on e2e-verify-code-submit-button , Verify Code value is 85900')
    })
    browser.pause(5000)
    helper.shot(browser, '12-after-login-modal.png', '01-Loginpage')
    helper.logout(browser)
    browser.pause(5000)
  },
  'step 03 -login header': function (browser) {
    let helper = browser.page.helper()
    let isMobile  = helper.isMobileBrowser(browser)
    browser.perform(function () {
      // setTimeout(function () {
        if ( isMobile.includes(true) ) {
          console.log('----------------isMobile : True')
          browser.waitForElementVisible('[e2e-nav-button]', 10000)
          browser.click('[e2e-nav-button]')
          helper.shot(browser, 'nav-button.png',  '01-Loginpage')
          browser.waitForElementVisible('[e2e-login-mobile]', 10000)
          browser.click('[e2e-login-mobile]')
          browser.pause(3000)
          helper.shot(browser, '13-login-sidebar-btn.png',  '01-Loginpage')
        } else if ( isMobile.includes(false) )  {
          console.log('----------------isMobile : False')
          browser.waitForElementVisible('[e2e-login-desktop]', 10000 ,false)
          browser.click('[e2e-login-desktop]')
          helper.shot(browser, '13-login-header-btn.png', '01-Loginpage')
        }
      // }, 10000)
    })
    browser.end()
  }
}
