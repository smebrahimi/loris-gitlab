module.exports = {
  before: function (browser) {
    let helper, isLocalBrowser
    helper = browser.page.helper()
    browser.perform(function () {
      isLocalBrowser = helper.isLocalBrowser(browser)
      if (isLocalBrowser) {
        browser.resizeWindow(375, 767)
      }
    })
  },
  'step 1 -Profile Page': function (browser) {
    let helper, isMobileBrowser, devServer
    helper = browser.page.helper()
    devServer = helper.getBaseUrl(browser)
    let userName = 'user name ' + Date.now()
    browser.url(devServer, function(result) {
      if ( result.status == -1) {
        console.log( devServer + 'page not found');
        browser.end()
      }
    });
    browser.perform(function () {
      isMobileBrowser = helper.isMobileBrowser(browser)
    })
    browser.waitForElementVisible('body', 10000)
    helper.login(browser)
    browser.pause(10000)
    helper.shot(browser, '01-init-home-page.png', '03-Profile')
    browser.perform(function () {
      console.log('isMobileBrowser : ' + isMobileBrowser)
      if (isMobileBrowser.includes(true)) {
        console.log('-------------isMobileBrowser-----------------')
        browser.waitForElementVisible('[e2e-nav-button]', 3000)
        browser.click('[e2e-nav-button]')
        browser.pause(3000)
        helper.shot(browser, 'nav-button.png', '03-Profile')
        browser.waitForElementVisible('[e2e-profile-s]', 3000)
        browser.click('[e2e-profile-s]')
        browser.pause(3000)
        helper.shot(browser, '02-Profile.png', '03-Profile')
      } else if (isMobileBrowser.includes(false)) {
        browser.waitForElementVisible('[e2e-profile-menu]', 5000, false)
        browser.click('[e2e-profile-menu]')
        helper.shot(browser, 'menu.png', '03-Profile')
        browser.waitForElementVisible('[e2e-profile]', 3000)
        browser.click('[e2e-profile]')
        browser.pause(3000)
        helper.shot(browser, '02-Profile.png', '03-Profile')
      }
    })
    browser.waitForElementVisible('[e2e-username-edit]', 3000)
    browser.click('[e2e-username-edit]')
    browser.pause(3000)
    helper.shot(browser, '03-username.png', '03-Profile')

    browser.waitForElementVisible('[e2e-username-input]', 3000)
    browser.setValue('[e2e-username-input]', 'test')
    browser.pause(3000)
    helper.shot(browser, '04-username-before-cancel.png', '03-Profile')

    browser.waitForElementVisible('[e2e-cancel-profile-modal]', 3000)
    browser.click('[e2e-cancel-profile-modal]')
    browser.pause(3000)
    helper.shot(browser, '05-username-cancel.png', '03-Profile')

    browser.waitForElementVisible('[e2e-username-edit]', 3000)
    browser.click('[e2e-username-edit]')
    browser.pause(3000)
    helper.shot(browser, '06-username-after-cancel.png', '03-Profile')

    browser.waitForElementVisible('[e2e-username-input]', 3000)
    browser.clearValue('[e2e-username-input]')
    browser.setValue('[e2e-username-input]', userName)
    browser.pause(3000)
    helper.shot(browser, '07-username-input-test.png', '03-Profile')

    browser.waitForElementVisible('[e2e-username-save]', 3000)
    browser.click('[e2e-username-save]')
    browser.pause(3000)
    helper.shot(browser, '08-test-username-save.png', '03-Profile')

    browser.waitForElementVisible('[e2e-username-edit]', 3000)
    browser.click('[e2e-username-edit]')
    browser.waitForElementVisible('[e2e-cancel-profile-modal]', 3000)
    browser.click('[e2e-cancel-profile-modal]')
    browser.pause(3000)
    helper.shot(browser, '09-username-cancel.png', '03-Profile')
    helper.logout(browser)
    browser.end()
  }
}
