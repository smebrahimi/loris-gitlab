module.exports = {
  before: function (browser) {
    let helper, isLocalBrowser
    helper = browser.page.helper()
    browser.perform(function () {
      isLocalBrowser = helper.isLocalBrowser(browser)
      if (isLocalBrowser) {
        browser.resizeWindow(375, 767)
      }
    })
  },
  'step 1 - Exit page': function (browser) {
    let helper, isMobileBrowser, devServer
    helper = browser.page.helper()
    devServer = helper.getBaseUrl(browser)
    browser.url(devServer, function(result) {
      if ( result.status == -1) {
        console.log( devServer + 'page not found');
        browser.end()
      }
    });
    browser.perform(function () {
      isMobileBrowser = helper.isMobileBrowser(browser)
    })
    browser.waitForElementVisible('body', 10000)
    helper.login(browser)
    browser.pause(10000)
    helper.shot(browser, '01-init-home-page.png', '07-logout')
    browser.perform(function () {
      console.log('isMobileBrowser : ' + isMobileBrowser)
      if (isMobileBrowser.includes(true)) {
        console.log('-------------isMobileBrowser-----------------')
        browser.waitForElementVisible('[e2e-nav-button]', 3000)
        browser.click('[e2e-nav-button]')
        browser.pause(3000)
        helper.shot(browser, 'nav-button.png', '07-logout')
        browser.waitForElementVisible('[e2e-logout-s]', 5000)
        browser.click('[e2e-logout-s]')
        browser.pause(3000)
      } else if (isMobileBrowser.includes(false)) {
        browser.waitForElementVisible('[e2e-profile-menu]', 5000, false)
        browser.click('[e2e-profile-menu]')
        helper.shot(browser, 'menu.png', '07-logout')
        browser.waitForElementVisible('[e2e-logout]', 5000)
        browser.click('[e2e-logout]')
        browser.pause(3000)
      }
    })
    browser.waitForElementVisible('[e2e-logout-modal]', 3000, function () {
      helper.shot(browser, '03-logout-modal.png', '07-logout')
    })
    browser.pause(2000)
    browser.waitForElementVisible('[e2e-logout-cancel]', 3000)
    browser.click('[e2e-logout-cancel]')
    browser.pause(3000)
    helper.shot(browser, '04-logout-cancel.png', '07-logout')
    // ---------------------------------------------------------
    browser.perform(function () {
      if (isMobileBrowser.includes(true)) {
        console.log('-------------isMobileBrowser-----------------')
        browser.waitForElementVisible('[e2e-logout-s]', 5000)
        browser.click('[e2e-logout-s]')
        browser.pause(3000)
      } else if (isMobileBrowser.includes(false)) {
        browser.waitForElementVisible('[e2e-profile-menu]', 5000, false)
        browser.click('[e2e-profile-menu]')
        browser.waitForElementVisible('[e2e-logout]', 5000)
        browser.click('[e2e-logout]')
      }
    })
    browser.waitForElementVisible('[e2e-logout-modal]', 10000)
    browser.pause(2000)
    browser.waitForElementVisible('[e2e-logout-exit]', 3000)
    browser.click('[e2e-logout-exit]')
    browser.pause(3000)
    helper.shot(browser, '05-logout-exit.png', '07-logout')
    browser.end()
  }
}
