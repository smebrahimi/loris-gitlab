module.exports = {
  before: function (browser) {
    let helper, isLocalBrowser
    helper = browser.page.helper()
    browser.perform(function () {
      isLocalBrowser = helper.isLocalBrowser(browser)
      if (isLocalBrowser) {
        browser.resizeWindow(375, 767)
      }
    })
  },
  'step 1 -footer Page': function (browser) {
    let devServer, helper
    helper = browser.page.helper()
    devServer = helper.getBaseUrl(browser)
    browser.url(devServer, function(result) {
      if ( result.status == -1) {
        console.log( devServer + 'page not found');
        browser.end()
      }
    });
    browser.waitForElementVisible('body', 10000)
    browser.execute('window.scrollTo(0,document.body.scrollHeight);');
    browser.pause(3000)
    helper.shot(browser, '01-init-home-page.png', '09-Footer')

    browser.waitForElementVisible('[e2e-contactus-footer]', 10000)
    browser.click('[e2e-contactus-footer]')
    browser.pause(3000)
    helper.shot(browser, '04-init-contactus-footer-page.png', '09-Footer')
    browser.back()
    browser.pause(500)
    helper.shot(browser, '05-back-from-contactus.png', '09-Footer')
    browser.waitForElementVisible('[e2e-aboutus-footer]', 10000)
    browser.click('[e2e-aboutus-footer]')
    browser.pause(3000)
    helper.shot(browser, '06-init-aboutus-footer-page.png', '09-Footer')
    browser.back()
    browser.pause(5000)
    helper.shot(browser, '07-back-from-aboutus.png', '09-Footer')

    browser.waitForElementVisible('[e2e-cafebazar]', 50000, false)
    browser.getAttribute('[e2e-cafebazar]', 'href', function (result) {
      console.info(result)
      this.assert.equal(result.value, 'https://cafebazaar.ir/app/com.farakav.anten/?l=fa')
      this.url(result.value)
      this.waitForElementVisible('body', 50000, false, function () {}, 'cafe bazar Page Present')
      helper.shot(this, '08-init-cafe-bazar-page.png', '09-Footer')
      this.back()
    })
    browser.waitForElementVisible('body', 10000, false)
    browser.waitForElementVisible('[e2e-sibapp]', 5000, false, function () {}, 'App Store logo  Present')
    browser.getAttribute('[e2e-sibapp]', 'href', function (result) {
      console.info(result)
      this.assert.equal(result.value, 'https://new.sibapp.com/applications/anten?ios')
      this.url(result.value)
      this.waitForElementVisible('body', 30000, false, function () {}, 'App Store Page Present')
      helper.shot(this, '09-init-app-store-page.png', '09-Footer')
      this.back()
      this.waitForElementVisible('body', 10000, false, function () {}, 'return from App Store Page & body Present')
    })

    browser.url(devServer + '/tv')
    browser.pause(2000)
    browser.execute('window.scrollTo(0,document.body.scrollHeight);');
    helper.shot(browser, '02-init-tv-page.png', '09-Footer')

    browser.url(devServer + '/archive')
    browser.pause(2000)
    browser.execute('window.scrollTo(0,document.body.scrollHeight);');
    helper.shot(browser, '03-init-archive-page.png', '09-Footer')
    browser.end()
  }
}
