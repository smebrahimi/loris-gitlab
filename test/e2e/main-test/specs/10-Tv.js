module.exports = {
  before: function (browser) {
    let helper, isLocalBrowser
    helper = browser.page.helper()
    browser.perform(function () {
      isLocalBrowser = helper.isLocalBrowser(browser)
      if (isLocalBrowser) {
        browser.resizeWindow(375, 767)
      }
    })
  },
  'step 1 -Tv page': function (browser) {
    let helper, isMobileBrowser, devServer
    helper = browser.page.helper()
    devServer = helper.getBaseUrl(browser)
    browser.url(devServer, function(result) {
      if ( result.status == -1) {
        console.log( devServer + 'page not found');
        browser.end()
      }
    });
    browser.perform(function () {
      isMobileBrowser = helper.isMobileBrowser(browser)
    })
    browser.waitForElementVisible('body', 10000)
    helper.login(browser)
    browser.pause(1000)
    browser.perform(function () {
      isMobileBrowser = helper.isMobileBrowser(browser)
    })
    browser.waitForElementVisible('body', 10000)
    helper.shot(browser, '1-1-init-home-page.png', '10-tv')
    browser.perform(function () {
      console.log('isMobileBrowser : ' + isMobileBrowser)
      if (isMobileBrowser.includes(true)) {
        console.log('-------------isMobileBrowser-----------------')
        browser.waitForElementVisible('[e2e-nav-button]', 10000)
        browser.click('[e2e-nav-button]')
        browser.pause(3000)
        helper.shot(browser, 'nav-button.png', '10-tv')
        browser.waitForElementVisible('[e2e-tv-s]', 10000)
        browser.click('[e2e-tv-s]')
        browser.pause(3000)
        helper.shot(browser, '1-2-e2e-tv.png', '10-tv')
      } else if (isMobileBrowser.includes(false)) {
        console.log('-------------isnotMobileBrowser-----------------')
        browser.waitForElementVisible('[e2e-tv]', 5000)
        browser.click('[e2e-tv]')
        browser.pause(3000)
        helper.shot(browser, '1-2-e2e-tv.png', '10-tv')
      }
    })
  },
  'step 2 - search ': function (browser) {
    let helper = browser.page.helper()
    let element = browser.page.element()
    browser.getElementSize('body' ,function (result) {
      console.log('----------------res is :' + result.value.width)
      if (result.value.width <= 991) {
        browser.waitForElementVisible('[e2e-program-list]', 10000 , false)
        helper.shot(browser, '2-1-beforeClickOnProgramList.png', '10-tv')
        browser.click('[e2e-program-list]')
        helper.shot(browser, '2-2-AfterClickOnProgramList.png', '10-tv')
      } else {
        console.log('----------------isMobile : False')
      }
    })
    browser.waitForElementVisible('[e2e-search-program]', 10000 , false)
    browser.click('[e2e-search-program]')
    browser.setValue('[e2e-search-program]', 'اذان')
    browser.pause(2000)
    helper.shot(browser, '2-3-searchText.png', '10-tv')

    element.waitForElementVisible('[e2e-result-0]', 90000 , false)
    element.click('[e2e-result-0]')
    browser.pause(2000)
    helper.shot(browser, '2-4-playSearchResult.png', '10-tv')

    browser.waitForElementVisible('[e2e-return-live]', 10000 , false)
    browser.click('[e2e-return-live]')
    browser.pause(2000)
    helper.shot(browser, '2-5-returnToLive.png', '10-tv')

    // remove search---------------------------------------------------------------------------------------------------
    browser.click('[e2e-search-program]')
    browser.clearValue('[e2e-search-program]')
    browser.pause(3000)
    helper.shot(browser, '2-6-cancelSearch.png', '10-tv')
  },
  'step 3 - calendar ': function (browser) {
    let helper = browser.page.helper()
    browser.waitForElementVisible('[e2e-calendar]', 10000 , false)
    browser.click('[e2e-calendar]')
    browser.pause(2000)
    helper.shot(browser, '3-1-calendar.png', '10-tv')
    browser.useXpath()
    browser.waitForElementVisible('//td[@data-subtract="1"]', 10000 , false)
    browser.click('//td[@data-subtract="1"]')
    browser.pause(2000)
    browser.useCss()
    browser.click('[e2e-calendar]')
    helper.shot(browser, '3-2-click-td1.png', '10-tv')
    browser.useXpath()
    browser.click('//td[@data-subtract="1"]')
    browser.useCss()
    browser.waitForElementVisible('div.programs-list.col-12.mt-2 > div:nth-child(4) > div:nth-child(1)', 10000 , false)
    browser.click('div.programs-list.col-12.mt-2 > div:nth-child(4) > div:nth-child(1)')
    browser.pause(2000)
    helper.shot(browser, '3-3-click-item1.png', '10-tv')
    browser.pause(2000)
    browser.waitForElementVisible('[e2e-return-live]', 10000 , false)
    browser.click('[e2e-return-live]')
    browser.pause(2000)
    helper.shot(browser, '3-4-returnToLive.png', '10-tv')

  },
  'step 4 - tv chanel': function (browser) {
    let isLocalBrowser
    let helper = browser.page.helper()
    let element = browser.page.element()
    browser.refresh()
    browser.perform(function () {
      isLocalBrowser = helper.isLocalBrowser(browser)
      if (isLocalBrowser) {
        browser.useXpath()
        //---------------tv1
        // browser.waitForElementVisible('//span[contains(text(),"شبکه یک")]', 10000, false)
        // browser.moveToElement('//span[contains(text(),"شبکه یک")]', 10, 10)
        // browser.click('//span[contains(text(),"شبکه یک")]')
        // browser.pause(10000)
        helper.shot(browser, '4-1-Tv1.png', '10-tv')
        browser.pause(5000)
        //---------------tv2
        browser.waitForElementVisible('//span[contains(text(),"شبکه دو")]', 10000, false, function (result) {
          if (result.value){
            browser.moveToElement('//span[contains(text(),"شبکه دو")]', 10, 10)
            browser.click('//span[contains(text(),"شبکه دو")]')
            browser.pause(10000)
            helper.shot(browser, '4-2-Tv2.png', '10-tv')
            browser.pause(2000)
          }
        } , 'shabake 2 is visible')

        //---------------tv3
        browser.waitForElementVisible('//span[contains(text(),"شبکه سه")]', 10000, false, function (result) {
          if (result.value){
            browser.moveToElement('//span[contains(text(),"شبکه سه")]', 10, 10)
            browser.click('//span[contains(text(),"شبکه سه")]')
            browser.pause(10000)
            helper.shot(browser, '4-3-Tv3.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake 3 is visible')

        //---------------tv4
        browser.waitForElementVisible('//span[contains(text(),"شبکه چهار")]', 10000, false, function (result) {
          if (result.value){
            browser.moveToElement('//span[contains(text(),"شبکه چهار")]', 10, 10)
            browser.click('//span[contains(text(),"شبکه چهار")]')
            browser.pause(10000)
            helper.shot(browser, '4-4-Tv4.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake 4 is visible')

        //---------------tvTehran
        browser.waitForElementVisible('//span[contains(text(),"شبکه تهران")]', 10000, false, function (result) {
          if (result.value){
            browser.moveToElement('//span[contains(text(),"شبکه تهران")]', 10, 10)
            browser.click('//span[contains(text(),"شبکه تهران")]')
            browser.pause(10000)
            helper.shot(browser, '4-5-TvTehran.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake tehran is visible')

        //---------------tvKhabar
        browser.waitForElementVisible('//span[contains(text(),"شبکه خبر")]', 10000, false , function (result) {
          if (result.value){
            browser.moveToElement('//span[contains(text(),"شبکه خبر")]', 10, 10)
            browser.click('//span[contains(text(),"شبکه خبر")]')
            browser.pause(10000)
            helper.shot(browser, '4-6-TvKhabar.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Khabar is visible')

        //---------------TvVarzesh
        browser.waitForElementVisible('//span[contains(text(),"شبکه ورزش")]', 10000, false , function (result) {
          if (result.value){
            browser.click('//span[contains(text(),"شبکه ورزش")]')
            browser.pause(10000)
            helper.shot(browser, '4-7-TvVarzesh.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Varzesh is visible')

        //---------------tvIfilm
        browser.waitForElementVisible('//span[contains(text(),"شبکه آی فیلم")]', 10000, false , function (result) {
          if (result.value){
            browser.click('//span[contains(text(),"شبکه آی فیلم")]')
            browser.pause(10000)
            helper.shot(browser, '4-8-TvIfilm.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Ifilm is visible')

        //---------------TvNasim
        browser.waitForElementVisible('//span[contains(text(),"شبکه نسیم")]', 10000, false , function (result) {
          if (result.value){
            browser.click('//span[contains(text(),"شبکه نسیم")]')
            browser.pause(10000)
            helper.shot(browser, '4-9-TvNasim.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Nasim is visible')

        //---------------TvNemayesh
        browser.waitForElementVisible('//span[contains(text(),"شبکه نمایش")]', 10000, false , function (result) {
          if (result.value){
            browser.click('//span[contains(text(),"شبکه نمایش")]')
            browser.pause(10000)
            helper.shot(browser, '4-10-TvNemayesh.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Nemayesh is visible')

        //---------------TvTamasha
        browser.waitForElementVisible('//span[contains(text(),"شبکه تماشا")]', 10000, false , function (result) {
          if (result.value){
            browser.click('//span[contains(text(),"شبکه تماشا")]')
            browser.pause(10000)
            helper.shot(browser, '4-11-TvTamasha.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Tamasha is visible')

        //---------------TvOfogh
        browser.waitForElementVisible('//span[contains(text(),"شبکه افق")]', 10000, false , function (result) {
          if (result.value){
            browser.click('//span[contains(text(),"شبکه افق")]')
            browser.pause(10000)
            helper.shot(browser, '4-12-TvOfogh.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Ofogh is visible')

        //---------------TvQoran
        browser.waitForElementVisible('//span[contains(text(),"شبکه قرآن")]', 10000, false , function (result) {
          if (result.value){
            browser.click('//span[contains(text(),"شبکه قرآن")]')
            browser.pause(10000)
            helper.shot(browser, '4-13-TvQoran.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Qoran is visible')

        //---------------TvPooya
        browser.waitForElementVisible('//span[contains(text(),"شبکه پویا")]', 10000, false , function (result) {
          if (result.value){
            browser.click('//span[contains(text(),"شبکه پویا")]')
            browser.pause(10000)
            helper.shot(browser, '4-14-TvPooya.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Pooya is visible')

        //---------------TvOmid
        browser.waitForElementVisible('//span[contains(text(),"شبکه امید")]', 10000, false , function (result) {
          if (result.value){
            browser.click('//span[contains(text(),"شبکه امید")]')
            browser.pause(10000)
            helper.shot(browser, '4-15-TvOmid.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Omid is visible')

        //---------------TvIranKala
        element.waitForElementVisible('//span[contains(text(),"شبکه ایران کالا")]', 10000, false , function (result) {
          if (result.value){
            element.moveToElement('//span[contains(text(),"شبکه ایران کالا")]', 10, 10)
            element.click('//span[contains(text(),"شبکه ایران کالا")]')
            browser.pause(10000)
            helper.shot(browser, '4-16-TvIranKala.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake IranKala is visible')

        //---------------TvMostanad
        element.waitForElementVisible('//span[contains(text(),"شبکه مستند")]', 10000, false , function (result) {
          if (result.value){
            element.moveToElement('//span[contains(text(),"شبکه مستند")]', 10, 10)
            element.click('//span[contains(text(),"شبکه مستند")]')
            browser.pause(10000)
            helper.shot(browser, '4-17-TvMostanad.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Mostanad is visible')

        //---------------TvShoma
        element.waitForElementVisible('//span[contains(text(),"شبکه شما")]', 10000, false , function (result) {
          if (result.value){
            element.moveToElement('//span[contains(text(),"شبکه شما")]', 10, 10)
            element.click('//span[contains(text(),"شبکه شما")]')
            browser.pause(10000)
            helper.shot(browser, '4-18-TvShoma.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Shoma is visible')

        //---------------TvAmoozesh
        element.waitForElementVisible('//span[contains(text(),"شبکه آموزش")]', 10000, false , function (result) {
          if (result.value){
            element.moveToElement('//span[contains(text(),"شبکه آموزش")]', 10, 10)
            element.click('//span[contains(text(),"شبکه آموزش")]')
            browser.pause(10000)
            helper.shot(browser, '4-19-TvAmoozesh.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Amoozesh is visible')

        //---------------TvSalamat
        element.waitForElementVisible('//span[contains(text(),"شبکه سلامت")]', 10000, false , function (result) {
          if (result.value){
            element.moveToElement('//span[contains(text(),"شبکه سلامت")]', 10, 10)
            element.click('//span[contains(text(),"شبکه سلامت")]')
            browser.pause(10000)
            helper.shot(browser, '4-20-TvSalamat.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Salamat is visible')

        //---------------TvPressTv
        element.waitForElementVisible('//span[contains(text(),"شبکه پرس تی وی")]', 10000, false , function (result) {
          if (result.value){
            element.moveToElement('//span[contains(text(),"شبکه پرس تی وی")]', 10, 10)
            element.click('//span[contains(text(),"شبکه پرس تی وی")]')
            browser.pause(10000)
            helper.shot(browser, '4-21-TvPressTv.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake PressTv is visible')

        //---------------TvAlalam
        element.waitForElementVisible('//span[contains(text(),"شبکه العالم")]', 10000, false , function (result) {
          if (result.value){
            element.moveToElement('//span[contains(text(),"شبکه العالم")]', 10, 10)
            element.click('//span[contains(text(),"شبکه العالم")]')
            browser.pause(10000)
            helper.shot(browser, '4-22-TvAlalam.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Alalam is visible')

        //---------------TvKosar
        element.waitForElementVisible('//span[contains(text(),"شبکه الکوثر")]', 10000, false , function (result) {
          if (result.value){
            element.moveToElement('//span[contains(text(),"شبکه الکوثر")]', 10, 10)
            element.click('//span[contains(text(),"شبکه الکوثر")]')
            browser.pause(10000)
            helper.shot(browser, '4-23-TvKosar.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Kosar is visible')

        //---------------TvJamjam
        element.waitForElementVisible('//span[contains(text(),"شبکه جام جم")]', 10000, false , function (result) {
          if (result.value){
            element.moveToElement('//span[contains(text(),"شبکه جام جم")]', 10, 10)
            element.click('//span[contains(text(),"شبکه جام جم")]')
            browser.pause(10000)
            helper.shot(browser, '4-24-TvJamjam.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Jamjam is visible')

        //---------------TvSepehr
        element.waitForElementVisible('//span[contains(text(),"شبکه سپهر")]', 10000, false , function (result) {
          if (result.value){
            element.moveToElement('//span[contains(text(),"شبکه سپهر")]', 10, 10)
            element.click('//span[contains(text(),"شبکه سپهر")]')
            browser.pause(10000)
            helper.shot(browser, '4-25-TvSepehr.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Sepehr is visible')

        //---------------TvKhorasanRazavi
        element.waitForElementVisible('//span[contains(text(),"شبکه خراسان رضوی")]', 10000, false , function (result) {
          if (result.value){
            element.moveToElement('//span[contains(text(),"شبکه خراسان رضوی")]', 10, 10)
            element.click('//span[contains(text(),"شبکه خراسان رضوی")]')
            browser.pause(10000)
            helper.shot(browser, '4-26-TvKhorasanRazavi.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake KhorasanRazavi is visible')

        //---------------TvKhozestan
        element.waitForElementVisible('//span[contains(text(),"شبکه خوزستان")]', 10000, false , function (result) {
          if (result.value){
            element.moveToElement('//span[contains(text(),"شبکه خوزستان")]', 10, 10)
            element.click('//span[contains(text(),"شبکه خوزستان")]')
            browser.pause(10000)
            helper.shot(browser, '4-27-TvKhozestan.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Khozestan is visible')

        //---------------TvMazandaran
        element.waitForElementVisible('//span[contains(text(),"شبکه مازندران")]', 10000, false, function (result) {
          if (result.value){
            element.moveToElement('//span[contains(text(),"شبکه مازندران")]', 10, 10)
            element.click('//span[contains(text(),"شبکه مازندران")]')
            browser.pause(10000)
            helper.shot(browser, '4-28-TvMazandaran.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Mazandaran is visible')

        //---------------TvSahand
        element.waitForElementVisible('//span[contains(text(),"شبکه سهند")]', 10000, false , function (result) {
          if (result.value){
            element.moveToElement('//span[contains(text(),"شبکه سهند")]', 10, 10)
            element.click('//span[contains(text(),"شبکه سهند")]')
            browser.pause(10000)
            helper.shot(browser, '4-29-TvSahand.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Sahand is visible')

        //---------------TvBaran
        element.waitForElementVisible('//span[contains(text(),"شبکه باران")]', 10000, false , function (result) {
          if (result.value){
            element.moveToElement('//span[contains(text(),"شبکه باران")]', 10, 10)
            element.click('//span[contains(text(),"شبکه باران")]')
            browser.pause(10000)
            helper.shot(browser, '4-30-TvBaran.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Baran is visible')

        //---------------TvEsfahan
        element.waitForElementVisible('//span[contains(text(),"شبکه اصفهان")]', 10000, false , function (result) {
          if (result.value){
            element.moveToElement('//span[contains(text(),"شبکه اصفهان")]', 10, 10)
            element.click('//span[contains(text(),"شبکه اصفهان")]')
            browser.pause(10000)
            helper.shot(browser, '4-31-TvEsfahan.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Esfahan is visible')

        //---------------TvFars
        element.waitForElementVisible('//span[contains(text(),"شبکه فارس")]', 10000, false , function (result) {
          if (result.value){
            element.moveToElement('//span[contains(text(),"شبکه فارس")]', 10, 10)
            element.click('//span[contains(text(),"شبکه فارس")]')
            browser.pause(10000)
            helper.shot(browser, '4-32-TvFars.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Fars is visible')

        //---------------TvAnten
        element.waitForElementVisible('//span[contains(text(),"شبکه آنتن پلاس")]', 10000, false , function (result) {
          if (result.value){
            element.moveToElement('//span[contains(text(),"شبکه آنتن پلاس")]', 10, 10)
            element.click('//span[contains(text(),"شبکه آنتن پلاس")]')
            browser.pause(10000)
            helper.shot(browser, '4-33-TvAntenPlus.png', '10-tv')
            browser.pause(5000)
          }
        } ,'shabake Anten is visible')
      }
    })
    browser.end()
  }
}
