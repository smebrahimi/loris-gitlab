module.exports = {
  before: function (browser) {
    let helper, isLocalBrowser
    helper = browser.page.helper()
    browser.perform(function () {
      isLocalBrowser = helper.isLocalBrowser(browser)
      if (isLocalBrowser) {
        browser.resizeWindow(375, 767)
      }
    })
  },
  'step 1 -Program page': function (browser) {
    let helper, isMobileBrowser, devServer
    helper = browser.page.helper()
    devServer = helper.getBaseUrl(browser)
    browser.url(devServer, function(result) {
      if ( result.status == -1) {
        console.log( devServer + 'page not found');
        browser.end()
      }
    });
    browser.perform(function () {
      isMobileBrowser = helper.isMobileBrowser(browser)
    })
    browser.waitForElementVisible('body', 10000)
    helper.login(browser)
    browser.pause(1000)
    helper.shot(browser, '01-init-home-page.png', '02-Program')
    browser.perform(function () {
      console.log('isMobileBrowser : ' + isMobileBrowser)
      if (isMobileBrowser.includes(true)) {
        console.log('-------------isMobileBrowser-----------------')
        browser.waitForElementVisible('[e2e-nav-button]', 10000)
        browser.click('[e2e-nav-button]')
        browser.pause(3000)
        helper.shot(browser, 'nav-button.png', '02-Program')
        browser.waitForElementVisible('[e2e-live-s]', 10000)
        browser.click('[e2e-live-s]')
        browser.pause(3000)
        helper.shot(browser, '02-video-paly-list.png', '02-Program')
      } else if (isMobileBrowser.includes(false)) {
        console.log('-------------isnotMobileBrowser-----------------')
        browser.waitForElementVisible('[e2e-live]', 10000)
        browser.click('[e2e-live]')
        browser.pause(3000)
        helper.shot(browser, '02-video-paly-list.png', '02-Program')
      }
    })
  },
  'step 2 -ReportErorr': function (browser) {
    let helper = browser.page.helper()
    browser.waitForElementVisible('div > div:nth-child(1) > div.col-10.col-sm-11.float-left.p-0.pr-1 > div:nth-child(1) > a > div > div > div > div', 10000)
    browser.click('div > div:nth-child(1) > div.col-10.col-sm-11.float-left.p-0.pr-1 > div:nth-child(1) > a > div > div > div > div')
    browser.waitForElementVisible('[e2e-report-error-button]', 10000)
    browser.assert.visible('[e2e-report-error-button]', 'ReportErrorButton is visible')
    browser.click('[e2e-report-error-button]')
    browser.pause(2000)
    helper.shot(browser, '03-FailureReportModal-anten.png', '02-Program')
    browser.waitForElementVisible('[e2e-report-error-modal]', 50000, false)
    browser.waitForElementVisible('[e2e-cancel-report-modal]', 50000, false)
    browser.assert.visible('[e2e-cancel-report-modal]', 'ReportCancelButton is visible')
    browser.click('[e2e-cancel-report-modal]')
    browser.pause(2000)
    helper.shot(browser, '04-FailureReportClose-anten.png', '02-Program')
    // FailureReportLink---------------------------------------------------------------------------------------------------
    browser.waitForElementVisible('[e2e-report-error-button]', 10000, false)
    browser.assert.visible('[e2e-report-error-button]', 'ReportButton is visible')
    browser.click('[e2e-report-error-button]')
    browser.waitForElementVisible('[e2e-error-dropdown]', 10000, false)
    browser.useXpath()
    browser.click('//a[contains(text(), "خرابی لینک")]')
    browser.useCss()
    browser.setValue('[e2e-report-error-modal-msg-input]', 'تست خرابی لینک ( لطفا در صورت بررسی این پیام را نادیده بگیرید )')
    browser.pause(2000)
    helper.shot(browser, '07-FailureReportLink-anten.png', '02-Program')
    browser.click('[e2e-report-error-modal-send-button]')
    browser.waitForElementVisible('.notifications', 10000, false, function () {
      helper.shot(this, '08-FailureReportLink-notif.png', '02-Program')
    })
    browser.pause(6000)
    // FailureReportAccess---------------------------------------------------------------------------------------------------
    browser.waitForElementVisible('[e2e-report-error-button]', 10000, false)
    browser.assert.visible('[e2e-report-error-button]', 'ReportButton is visible')
    browser.click('[e2e-report-error-button]')
    browser.waitForElementVisible('[e2e-error-dropdown]', 10000, false)
    browser.click('[e2e-error-dropdown]')
    browser.waitForElementVisible('[e2e-error-dropdown]', 10000, false)
    browser.useXpath()
    browser.click('//a[contains(text(), "عدم دسترسی")]')
    browser.useCss()
    browser.setValue('[e2e-report-error-modal-msg-input]', 'تست عدم دسترسی ( لطفا در صورت بررسی این پیام را نادیده بگیرید )')
    browser.pause(2000)
    helper.shot(browser, '09-FailureReportAccess-anten.png', '02-Program')
    browser.click('[e2e-report-error-modal-send-button]')
    browser.waitForElementVisible('.notifications', 10000, false, function () {
      helper.shot(this, '10-FailureReportAccess-notif.png', '02-Program')
    })
    browser.pause(6000)
  },
  'step 3 -scroll': function (browser) {
    let helper = browser.page.helper()
    let devServer = helper.getBaseUrl(browser)
    browser.url(devServer, function(result) {
      if ( result.status == -1) {
        console.log( devServer + 'page not found');
        browser.end()
      }
    });
    browser.waitForElementVisible('body', 10000)
    browser.getLocationInView('[e2e-contactus-footer]', function () {
      browser.pause(2000)
      helper.shot(browser, '11-scroll.png', '02-Program')
    })
    helper.logout(browser)
    browser.end()
  }
}
