module.exports = {
  before: function (browser) {
    let helper, isLocalBrowser
    helper = browser.page.helper()
    browser.perform(function () {
      isLocalBrowser = helper.isLocalBrowser(browser)
      if (isLocalBrowser) {
        browser.resizeWindow(375, 767)
      }
    })
  },
  'step 1 -Archive page': function (browser) {
    let helper, isMobileBrowser, devServer
    helper = browser.page.helper()
    devServer = helper.getBaseUrl(browser)
    browser.url(devServer, function(result) {
      if ( result.status == -1) {
        console.log( devServer + 'page not found');
        browser.end()
      }
    });
    browser.perform(function () {
      isMobileBrowser = helper.isMobileBrowser(browser)
    })
    browser.waitForElementVisible('body', 10000)
    helper.login(browser)
    browser.pause(1000)
    browser.perform(function () {
      console.log('isMobileBrowser : ' + isMobileBrowser)
      if (isMobileBrowser.includes(true)) {
        console.log('-------------isMobileBrowser-----------------')
        browser.waitForElementVisible('[e2e-nav-button]', 10000)
        browser.click('[e2e-nav-button]')
        browser.pause(3000)
        helper.shot(browser, 'nav-button.png', '12-Archive')
        browser.waitForElementVisible('[e2e-archive-s]', 10000)
        browser.click('[e2e-archive-s]')
        browser.pause(3000)
        helper.shot(browser, '01-archive.png', '12-Archive')
      } else if (isMobileBrowser.includes(false)) {
        console.log('-------------isnotMobileBrowser-----------------')
        browser.waitForElementVisible('[e2e-archive]', 10000)
        browser.click('[e2e-archive]')
        browser.pause(3000)
        helper.shot(browser, '01-archive.png', '12-Archive')
      }
    })
  },
  // 'step 2 -select program': function (browser) {
    // let helper = browser.page.helper()
    // let element = browser.page.element()
    // let player = browser.page.player()
    //
    // element.waitForElementVisible('@SelectFirstProgram', 10000)
    // element.assert.visible('@SelectFirstProgram', 'First Program is visible')
    // element.click('@SelectFirstProgram')
    // browser.pause(2000)
    // helper.shot(browser, '01-select program.png', '12-Archive')
    // // -------------------------------paly & pause with paly & pause Button---------------------------
    // player.waitForElementVisible('@PlayerPlayerPlayBox', 10000, false)
    // player.assert.elementPresent('@PlayerPlayerPlayBox', 10000)
    // browser.pause(5000)
    // helper.shot(browser, '03-Init-paly-and-pause.png', '12-Archive')
    // player.hoverOnVideoPlayerContainer(browser)
    // player.waitForElementVisible('@PlayerPlayerPauseButton', 10000, false)
    // player.click('@PlayerPlayerPauseButton')
    // browser.pause(2000)
    // helper.shot(browser, '04-pauseVideoButton.png', '12-Archive')
    // // -------------------------------Paly & Pause With Paly & Pause click------------------------------
    // player.hoverOnVideoPlayerContainer(browser)
    // player.click('@PlayerPlayerPlayBox')
    // helper.shot(browser, '05-pauseVideoClick.png', '12-Archive')
    // player.hoverOnVideoPlayerContainer(browser)
    // player.click('@PlayerPlayerPlayBox')
    // browser.pause(2000)
    // helper.shot(browser, '06-playVideoClick.png', '12-Archive')
    // // -------------------------------------Video Quality Level------------------------------------------
    // player.hoverOnVideoPlayerContainer(browser)
    // player.click('@PlayerPlayerPlayBox')
    // browser.useXpath()
    // player.waitForElementVisible('//span[@class="vjs-resolution-button-label"]', 15000, false)
    // player.click('//span[@class="vjs-resolution-button-label"]')
    // browser.pause(2000)
    // helper.shot(browser, '07-hoverOnVideoPlayerResolutionMenu.png', '12-Archive')
    // browser.useCss()
    // player.hoverOnVideoPlayerContainer(browser)
    // player.click('@PlayerPlayerPlayBox')
    // browser.useXpath()
    // player.click('//span[@class="vjs-resolution-button-label"]')
    // player.click('//span[@class="vjs-menu-item-text"][contains(text(),"اتوماتیک")]')
    // browser.pause(2000)
    // helper.shot(browser, '08-hoverOnVideoPlayerResolutionSelected.png', '12-Archive')
    // //------------------------------270p------------------------------------
    // player.hoverOnVideoPlayerContainer(browser)
    // browser.useCss()
    // player.click('@PlayerPlayerPlayBox')
    // browser.useXpath()
    // browser.getValue('//span[contains(text(),"270p")]' , function(result) {
    //   browser.perform(function () {
    //     if(result.status == 0){
    //       browser.useXpath()
    //       player.click('//span[@class="vjs-resolution-button-label"]')
    //       player.click('//span[contains(text(),"270p")]')
    //       browser.pause(2000)
    //       helper.shot(browser, '09-hoverOnVideoPlayerResolution270.png', '12-Archive')
    //       console.log('270p visible')
    //     } else {
    //       console.log('270p no visible')
    //     }
    //   })
    // });
    // //-------------------------------480p-----------------------------------
    // player.hoverOnVideoPlayerContainer(browser)
    // browser.useCss()
    // player.click('@PlayerPlayerPlayBox')
    // browser.useXpath()
    // browser.getValue('//span[contains(text(),"480p")]' , function(result) {
    //   browser.perform(function () {
    //     if(result.status == 0){
    //       browser.useXpath()
    //       player.click('//span[@class="vjs-resolution-button-label"]')
    //       player.click('//span[contains(text(),"480p")]')
    //       browser.pause(2000)
    //       helper.shot(browser, '10-hoverOnVideoPlayerResolution480.png', '12-Archive')
    //       console.log('480p visible')
    //     } else {
    //       console.log('480p no visible')
    //     }
    //   })
    // });
    // //--------------------------------540p----------------------------------
    // player.hoverOnVideoPlayerContainer(browser)
    // browser.useCss()
    // player.click('@PlayerPlayerPlayBox')
    // browser.useXpath()
    // browser.getValue('//span[contains(text(),"540p")]' , function(result) {
    //   browser.perform(function () {
    //     if(result.status == 0){
    //       browser.useXpath()
    //       player.click('//span[@class="vjs-resolution-button-label"]')
    //       player.click('//span[contains(text(),"540p")]')
    //       browser.pause(2000)
    //       helper.shot(browser, '11-hoverOnVideoPlayerResolution540.png', '12-Archive')
    //       console.log('540p visible')
    //     } else {
    //       console.log('540p no visible')
    //     }
    //   })
    // });
    // //-------------------------------720p----------------------------------
    // player.hoverOnVideoPlayerContainer(browser)
    // browser.useCss()
    // player.click('@PlayerPlayerPlayBox')
    // browser.useXpath()
    // browser.getValue('//span[contains(text(),"720p")]', function(result) {
    //   browser.perform(function () {
    //     if(result.status == 0){
    //       browser.useXpath()
    //       player.click('//span[@class="vjs-resolution-button-label"]')
    //       player.click('//span[contains(text(),"720p")]')
    //       browser.pause(2000)
    //       helper.shot(browser, '12-hoverOnVideoPlayerResolution720.png', '12-Archive')
    //       console.log('720p visible')
    //     } else {
    //       console.log('720p no visible')
    //     }
    //   })
    // });
    // // ------------------------------------Screen size------------------------------------
    // player.hoverOnVideoPlayerContainer(browser)
    // player.click('@PlayerPlayerFullScreenButton')
    // helper.shot(browser, '13-FullScreenButton.png', '12-Archive')
    // player.hoverOnVideoPlayerContainer(browser)
    // player.click('@PlayerPlayerExitFullScreenButton')
    // browser.pause(2000)
    // helper.shot(browser, '14-ExitFullScreenButton.png', '12-Archive')
    // // -----------------------------------Volume-----------------------------------------
    // player.hoverOnVideoPlayerContainer(browser)
    // player.click('@PlayerPlayerMuteButton')
    // browser.pause(2000)
    // helper.shot(browser, '15-PlayerMuteButton.png', '12-Archive')
    // player.hoverOnVideoPlayerContainer(browser)
    // player.click('@PlayerPlayerUnmuteButton')
    // browser.pause(2000)
    // helper.shot(browser, '16-PlayerUnmuteButton.png', '12-Archive')
    // -----------------------------------Volume Level-------------------------------------*/
    // player.assert.elementPresent('@PlayerPlayerPlayBox', 10000)
    // browser.useXpath()
    // player.assert.elementPresent('//span[@class="vjs-resolution-button-label"]', 10000)
    // browser.pause(3000)
    // browser.execute(function () {
    //   let $video = jQuery('.video-js')
    //
    //   setInterval(function () {
    //     $video.removeClass('vjs-user-inactive')
    //     $video.addClass('vjs-user-active')
    //   }, 2000)
    //   // jQuery('.vjs-volume-menu-button .vjs-menu .vjs-menu-content').css({
    //   //     width: '2.9em'
    //   // });
    //   jQuery('.vjs-volume-menu-button .vjs-menu-content').show().css({
    //     width: '2.9em'
    //   })
    //   helper.shot(browser, '17-Init-Volume-Level.png', '12-Archive')
    // }, [], function () {
    //   player.PAUSE(15000)
    //   browser.useXpath()
    //   player.assert.visible('//span[@class="vjs-resolution-button-label"]')
    //   player.seekOnVolumeBar(0.5, function () {
    //     browser.pause(1000)
    //     helper.shot(browser, '18-Set-Volume-0.2.png', '12-Archive')
    //     player.PAUSE()
    //   })
    // })
  // },
  'step 3 -Search': function (browser) {
    let helper = browser.page.helper()
    browser.waitForElementVisible('[e2e-archive-search-input]', 10000)
    browser.assert.visible('[e2e-archive-search-input]', 'archive search input is visible')
    browser.click('[e2e-archive-search-input]')
    browser.setValue('[e2e-archive-search-input]', 'پرسپولیس')
    browser.pause(2000)
    helper.shot(browser, '02-searchText.png', '12-Archive')
    // remove search---------------------------------------------------------------------------------------------------
    browser.waitForElementVisible('[e2e-archive-search-cancel]', 10000)
    browser.assert.visible('[e2e-archive-search-cancel]', 'archive search cancel is visible')
    browser.click('[e2e-archive-search-cancel]')
    browser.pause(3000)
    helper.shot(browser, '03-cancelSearch.png', '12-Archive')
  },
  'step 4 -scroll': function (browser) {
    let helper = browser.page.helper()
    let devServer = helper.getBaseUrl(browser)
    browser.url(devServer + '/archive')
    browser.waitForElementVisible('body', 10000)
    browser.getLocationInView('[e2e-contactus-footer]', function () {
      helper.shot(browser, '04-scroll.png', '12-Archive')
    })
    helper.logout(browser)
    browser.end()
  }
}
