module.exports = {
  before: function (browser) {
    let helper, isLocalBrowser
    helper = browser.page.helper()
    browser.perform(function () {
      isLocalBrowser = helper.isLocalBrowser(browser)
      if (isLocalBrowser) {
        browser.resizeWindow(375, 767)
      }
    })
  },
  'step 1 -follow': function (browser) {
    let helper = browser.page.helper()
    let isTabletBrowser = helper.isTabletBrowser(browser)
    let isLocalBrowser = helper.isLocalBrowser(browser)
    let devServer = helper.getBaseUrl(browser)
    browser.url(devServer, function(result) {
      if ( result.status == -1) {
        console.log( devServer + 'page not found');
        browser.end()
      }
    });
    browser.waitForElementVisible('body', 15000)
    browser.perform(function () {
      if (isTabletBrowser.includes(true)) {
        console.log(isTabletBrowser)
        if (!isLocalBrowser) {
          console.log(isLocalBrowser)
          browser.waitForElementVisible('div > div:nth-child(1) > div.col-10.col-sm-11.float-left.p-0.pr-1 > div:nth-child(1) > a > div > div > div.card-body', 10000)
          browser.click('div > div:nth-child(1) > div.col-10.col-sm-11.float-left.p-0.pr-1 > div:nth-child(1) > a > div > div > div.card-body')

          browser.waitForElementVisible('[e2e-share-links]', 10000)
          browser.click('[e2e-share-links]')
          // ------------------------------facebook-------------------------------------
          browser.waitForElementVisible('[e2e-facebook-m]', 10000, false)
          browser.getAttribute('[e2e-facebook-m]', 'href', function (result) {
            console.info(result)
            this.url(result.value)
            this.waitForElementVisible('body', 10000, function () {
              helper.shot(browser, '01-page-facebook.png', '11-Follow')
            })
            this.back()
            this.pause(10000)
            this.waitForElementVisible('body', 10000, false)
          })
          browser.pause(3000)
          // ---------------------------------google----------------------------------
          browser.waitForElementVisible('[e2e-share-links]', 10000)
          browser.click('[e2e-share-links]')
          browser.waitForElementVisible('[e2e-google-m]', 10000, false)
          browser.getAttribute('[e2e-google-m]', 'href', function (result) {
            console.info(result)
            this.url(result.value)
            this.waitForElementVisible('body', 10000, function () {
              helper.shot(browser, '02-page-google.png', '11-Follow')
            })
            this.back()
            this.pause(10000)
            this.waitForElementVisible('body', 10000, false)
          })
          browser.pause(3000)
          // -----------------------------------twitter--------------------------------
          browser.waitForElementVisible('[e2e-share-links]', 10000)
          browser.click('[e2e-share-links]')
          browser.waitForElementVisible('[e2e-twitter-m]', 10000, false)
          browser.getAttribute('[e2e-twitter-m]', 'href', function (result) {
            console.info(result)
            this.url(result.value)
            this.waitForElementVisible('body', 10000, function () {
              helper.shot(browser, '03-page-twitter.png', '11-Follow')
            })
            this.back()
            this.pause(10000)
            this.waitForElementVisible('body', 10000, false)
          })
          browser.pause(3000)
          // -----------------------------------telegram-------------------------------
          browser.waitForElementVisible('[e2e-share-links]', 10000)
          browser.click('[e2e-share-links]')
          browser.waitForElementVisible('[e2e-telegram-m]', 10000, false)
          browser.getAttribute('[e2e-telegram-m]', 'href', function (result) {
            console.info(result)
            this.url(result.value)
            this.waitForElementVisible('body', 10000, function () {
              helper.shot(browser, '04-page-telegram.png', '11-Follow')
            })
            this.back()
            this.pause(10000)
            this.waitForElementVisible('body', 10000, false)
          })
          browser.pause(5000)
        }
      }
      else {
        if (!isLocalBrowser) {
          browser.waitForElementVisible('div > div:nth-child(1) > div.col-10.col-sm-11.float-left.p-0.pr-1 > div:nth-child(1) > a > div > div > div.card-body', 10000)
          browser.click('div > div:nth-child(1) > div.col-10.col-sm-11.float-left.p-0.pr-1 > div:nth-child(1) > a > div > div > div.card-body')
          // ------------------------------facebook-------------------------------------
          browser.waitForElementVisible('[e2e-facebook]', 10000, false)
          browser.getAttribute('[e2e-facebook]', 'href', function (result) {
            console.info(result)
            this.url(result.value)
            this.waitForElementVisible('body', 10000, function () {
              helper.shot(browser, '01-page-facebook.png', '11-Follow')
            })
            this.back()
            this.pause(10000)
            this.waitForElementVisible('body', 10000, false)
          })
          browser.pause(3000)
          // ---------------------------------google----------------------------------
          browser.waitForElementVisible('[e2e-google]', 10000, false)
          browser.getAttribute('[e2e-google]', 'href', function (result) {
            console.info(result)
            this.url(result.value)
            this.waitForElementVisible('body', 10000, function () {
              helper.shot(browser, '02-page-google.png', '11-Follow')
            })
            this.back()
            this.pause(10000)
            this.waitForElementVisible('body', 10000, false)
          })
          browser.pause(3000)
          // -----------------------------------twitter--------------------------------
          browser.waitForElementVisible('[e2e-twitter]', 10000, false)
          browser.getAttribute('[e2e-twitter]', 'href', function (result) {
            console.info(result)
            this.url(result.value)
            this.waitForElementVisible('body', 10000, function () {
              helper.shot(browser, '03-page-twitter.png', '11-Follow')
            })
            this.back()
            this.pause(10000)
            this.waitForElementVisible('body', 10000, false)
          })
          browser.pause(3000)
          // -----------------------------------telegram-------------------------------
          browser.waitForElementVisible('[e2e-telegram]', 10000, false)
          browser.getAttribute('[e2e-telegram]', 'href', function (result) {
            console.info(result)
            this.url(result.value)
            this.waitForElementVisible('body', 10000, function () {
              helper.shot(browser, '04-page-telegram.png', '11-Follow')
            })
            this.back()
            this.pause(10000)
            this.waitForElementVisible('body', 10000, false)
          })
          browser.pause(5000)
        }
      }
    })
    browser.end()
  }
}
