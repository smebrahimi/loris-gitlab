# loris

> Anten.ir VUEJS client (optimized for tv and multiple clients)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

# tasks

- clear countDown Timer of the video

# E2E tests
-----------------------------
Firefox browser ------> npm run e2e-main-firfox

Chrome browser  ------> npm run e2e-main-chrome

Chrome headless browser  ------> npm run e2e-main-headless

- npm run e2e-m-login
- npm run e2e-m-program
- npm run e2e-m-profile
- npm run e2e-m-contact-us
- npm run e2e-m-about-us
- npm run e2e-m-logout
- npm run e2e-m-footer
- npm run e2e-m-follow
- npm run e2e-m-archive

npm run e2e-tv
-----------------------------

npm run e2e-hybrid
-----------------------------
