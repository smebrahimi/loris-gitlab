﻿var PushNotifications = (function () {
  let applicationServerPublicKey
  const baseUrl = 'https://venom.farakav.com/'
  const baseUrlAnten = 'https://api.anten.ir/v3.0/'
  const token = localStorage.getItem('token')
  let pushServiceWorkerRegistration

  function urlB64ToUint8Array (base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4)
    const base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/')

    const rawData = window.atob(base64)
    const outputArray = new Uint8Array(rawData.length)

    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i)
    }

    return outputArray
  }

  function isUserLogin () {
    if (!token) return
    fetch(baseUrlAnten + 'users/self', {
      headers: {
        Authorization: token
      }
    })
      .then(function (response) {
        if (response.status === 200) {
          registerPushServiceWorker()
        }
      })
      .catch(function () {
        // console.log('User not login: ' + error)
      })
  }

  function registerPushServiceWorker () {
    navigator.serviceWorker.register('static/push-notification/service-workers/push-service-worker18.js',
      {scope: 'static/push-notification/service-workers/push-service-worker/'})
      .then(function (serviceWorkerRegistration) {
        pushServiceWorkerRegistration = serviceWorkerRegistration
        subscribeForPushNotifications()
        // console.log('Push Service Worker has been registered successfully')
      }).catch(function () {
      // console.log('Push Service Worker registration has failed: ' + error)
      })
  }

  function subscribeForPushNotifications () {
    if (applicationServerPublicKey) {
      subscribeForPushNotificationsInternal()
    } else {
      fetch(baseUrl + 'v0.1/subscriptions/public-key')
        .then(function (response) {
          if (response.ok) {
            return response.text()
          } else {
            // console.log('Failed to retrieve Public Key')
          }
        }).then(function (applicationServerPublicKeyBase64) {
          applicationServerPublicKey = urlB64ToUint8Array(applicationServerPublicKeyBase64)
          // console.log('Successfully retrieved Public Key')
          subscribeForPushNotificationsInternal()
        }).catch(function () {
        // console.log('Failed to retrieve Public Key: ' + error)
        })
    }
  }

  function subscribeForPushNotificationsInternal () {
    pushServiceWorkerRegistration.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: applicationServerPublicKey
    })
      .then(function (pushSubscription) {
        var tags = []
        var pushData = {
          subscription: pushSubscription,
          tags: tags
        }

        fetch(baseUrl + 'v0.1/subscriptions', {
          method: 'POST',
          headers: {'Content-Type': 'application/json'},
          body: JSON.stringify(pushData)
        })
          .then(function (response) {
            if (response.ok) {
              return response.text()
              // console.log('Successfully subscribed for Push Notifications')
            } else {
              unsubscribeFromPushNotifications()
              // console.log('Failed to store the Push Notifications subscrition on server')
            }
          }).then(function (param) {
            const res = JSON.parse(param)
            updateUserAfterSubscribe(res.refId)
          }).catch(function () {
            unsubscribeFromPushNotifications()
            // console.log('Failed to store the Push Notifications subscrition on server: ' + error)
          })
      }).catch(function () {
        if (Notification.permission !== 'denied') {
          // console.log('Failed to subscribe for Push Notifications: ' + error)
        }
      })
  }

  function updateUserAfterSubscribe (refId) {
    const data = {
      VenomRefId: refId
    }
    fetch(baseUrlAnten + 'users/self/venomRefId', {
      method: 'PATCH',
      headers: {
        Authorization: token,
        'content-type': 'application/json'
      },
      body: JSON.stringify(data)
    })
      .then(function (response) {
        if (response.status === 200) {
          // console.log('Succesfully update user')
        } else {
          unsubscribeFromPushNotifications()
        }
      })
      .catch(function () {
        unsubscribeFromPushNotifications()
      })
  }

  function unsubscribeFromPushNotifications () {
    pushServiceWorkerRegistration.pushManager.getSubscription()
      .then(function (pushSubscription) {
        if (pushSubscription) {
          pushSubscription.unsubscribe()
            .then(function () {
              fetch(baseUrl + 'v0.1/subscriptions?endpoint=' + encodeURIComponent(pushSubscription.endpoint), {
                method: 'DELETE'
              })
                .then(function (response) {
                  if (response.ok) {
                    // writeToConsole('Successfully unsubscribed from Push Notifications')
                  } else {
                    // writeToConsole('Failed to discard the Push Notifications subscrition from server')
                  }
                }).catch(function () {
                  // writeToConsole('Failed to discard the Push Notifications subscrition from server: ' + error)
                })
            }).catch(function () {
              // writeToConsole('Failed to unsubscribe from Push Notifications: ' + error)
            })
        }
      })
  }

  return {
    initialize: function () {
      if (!'serviceWorker' in navigator) {
        // console.log('Service Workers are not supported')
        return
      }

      if (!'PushManager' in window) {
        // console.log('Push API not supported')
        return
      }

      // if the user has granted access before
      if (Notification.permission === 'granted') {
        // console.log('granted')
        return
      }
      isUserLogin()
    }
  }
})()

PushNotifications.initialize()
